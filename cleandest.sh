#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

#section for bm and dyu
langs="bm dyu"

for lang in $langs; do
  find ./src/routes/$lang/docs -type d -name resources -exec rm -fr {} +
  find ./src/routes/$lang/blog -mindepth 1 -type d -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "contact" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "slides" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "w_jl" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "w_jl_w100" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "udhr-morph" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "morph" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "freq" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "wtlex" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "carbon" -exec rm -fr {} +
  # find ./src/routes/$lang -type d -name "alpha-dyu" -exec rm -fr {} +
done

# langs="bci"
#
# for lang in $langs; do
#   true
#   # echo $lang
#   # find ./src/routes/$lang/docs -type d -name resources -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "about" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "blog" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "bambara-manual" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "animals" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "fontlist" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "money" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "alpha-dyu" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "wtlex" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "wtlex-doc" -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "kodi-w100" -exec rm -fr {} +
#   # find ./src/routes/$lang/docs -type f -name "*.svelte" -mindepth 1 -exec rm -frv {} +
#   # find ./src/routes/$lang/docs -type f -name "+page.js" -mindepth 1 -exec rm -frv {} +
#   #  find ./src/routes/$lang/contact -mindepth 1 -type d -exec rm -fr {} +
#   # find ./src/routes/$lang -type d -name "alpha-dyu" -exec rm -fr {} +
# done

#section for all files/langs; could include en as well (be careful)
find ./src/routes/fr -type d -name "udhr2" -exec rm -fr {} +
# create the json meta files while we are at it.
# not really cleaning but must run after po4a
# find ./src/routes/ -name alpha-dyu.yaml -exec sh -c 'yq -oj -I0 ".[] |= (.id = .letter)" "$1" > "${1%.yaml}.json"' _ {} \;
# find ./src/routes/ -name meta.yaml -exec sh -c 'yq -oj -I0 "$1" > "${1%.yaml}.json"' _ {} \;
find ./src/routes/ -name "*.yaml" -exec sh -c 'yq -oj -I0 "$1" > "${1%.yaml}.json"' _ {} \;
#this requires the id field
find ./src/routes/ -name alpha-dyu.yaml -exec sh -c 'yq -oj -I0 ".[] |= (.id = .letter)" "$1" > "${1%.yaml}.json"' _ {} \;
find ./src/lib/i18n/locales/ -name "*.yaml" -exec sh -c 'yq -oj "$1" > "${1%.yaml}.json"' _ {} \;

#section for bm dyu fr
langs="bm dyu fr"

for lang in $langs; do
  find ./src/routes/$lang -type d -name "wtlex-doc" -exec rm -fr {} +
  find ./src/routes/$lang -type d -name "bambara-manual" -exec rm -fr {} +
  find ./src/lib/i18n/locales -type f -name "$lang.yaml" -delete
done
