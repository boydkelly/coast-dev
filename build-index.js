import fs from 'fs';
import lunr from 'lunr';

const dataJson = fs.readFileSync('./src/lib/data.json', 'utf-8');
const data = JSON.parse(dataJson);

// Custom trimmer function for Latin extended characters
const myTrimmer = (token) => {
  return token.update(function (s) {
    return s.replace(
      /^[^a-zA-Z0-9\u00C0-\u02AF\u1E00-\u1EFF\u014B\u014C\u14B7\u14B8\s]+/,
      ''
    ).replace(
      /[^a-zA-Z0-9\u00C0-\u02AF\u1E00-\u1EFF\u014B\u014C\u14B7\u14B8\s]+$/,
      ''
    );
  });
};

lunr.Pipeline.registerFunction(myTrimmer, 'myTrimmer');

// Function to remove diacritics from a string
const removeDiacritics = str => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

// Preprocess the data by removing diacritics
const preprocessedData = data.docs.map(doc => ({
  id: doc.id,
  Text: removeDiacritics(doc.Text),
  // article: doc.article,
  examples: doc.examples,
}));

const idx = lunr(function () {
   this.pipeline.remove(lunr.trimmer);
   // this.pipeline.remove(lunr.stopWordFilter);
   this.pipeline.after(lunr.stemmer || lunr.stopWordFilter || null, myTrimmer);

  this.ref('id');
  this.field('Text');
  this.field('examples');

  preprocessedData.forEach((doc) => {
    this.add(doc);
  });
});

const lunrIndexJson = JSON.stringify(idx, null, 2);
fs.writeFileSync('src/lib/index.json', lunrIndexJson);
console.log('Lunr index prebuilt and saved to src/lib/index.json');

