#!/usr/bin/env python3
import sys
import os
import random

from os import path
from wordcloud import WordCloud

# Example base16 color scheme

base16_colors = [
"#08bdba",
"#3ddbd9",
"#78a9ff",
"#ee5396",
"#33b1ff",
"#ff7eb6",
"#42be65",
"#be95ff",
"#82cfff",
]

def base16_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    return random.choice(base16_colors)

# get data directory (using getcwd() is needed to support running example in generated IPython notebook)
d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

base = sys.argv[1]
text ="Mɔgɔkɔrɔbaw kaɲi ka kɛ ni ŋania ɲuman ye"
output = base
stop = "src/stop.dyu.txt"
#print (stop)
# Read the whole text.
#text = open(path.join(d, text)).read()
#text = open(text).read()

stopwords=set()
with open(stop, "r") as file:
    for line in file:
        stripped_line = line.strip()
        stopwords.add(stripped_line)

wc = WordCloud(background_color="black", 
               stopwords=stopwords,
               collocations=False,
               min_word_length=1,
               repeat=False,
               relative_scaling=.2,
               width=1500,
               height=500,
               font_path="./_resources/fonts/notosans-regular.ttf", 
               color_func=base16_color_func,  # Apply the custom color function
               max_words=50)

wc.generate(text)

wc_svg = wc.to_svg(embed_font=True)
f = open(output,"w+")
f.write(wc_svg)
f.close()

# Display the generated image:
# the matplotlib way:
#import matplotlib.pyplot as plt

# lower max_font_size
#plt.figure()
#plt.imshow(wc, interpolation="bilinear")
#plt.axis("off")
#plt.show()

