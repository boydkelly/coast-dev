#!/usr/bin/env python3
import sys
import os

from os import path
from wordcloud import WordCloud

base16_colors = [
    "#08bdba",
    "#3ddbd9",
    "#78a9ff",
    "#ee5396",
    "#33b1ff",
    "#ff7eb6",
    "#42be65",
    "#be95ff",
    "#82cfff",
]

def base16_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    return random.choice(base16_colors)

lang = sys.argv[1]
base = sys.argv[2]

# Determine the project root directory
project_root = path.abspath(path.join(path.dirname(__file__), '..'))

# Set the correct paths relative to the project root
input = path.join(project_root, "udhr/" + lang +"/" + base + "." + lang + ".txt")
output = path.join(project_root, "udhr/" + lang + "/images/udhr." + lang + ".svg")
stop = path.join(project_root, "udhr/stop." + lang + ".txt")
font_path = path.join(project_root, "_resources/fonts/notosans-regular.ttf")

print(input)
text = open(input).read()
#print (stop)
# Read the whole text.
#text = open(path.join(d, input).read())

stopwords=set()
with open(stop, "r") as file:
    for line in file:
        stripped_line = line.strip()
        stopwords.add(stripped_line)


wc = WordCloud(background_color="black", stopwords=stopwords,
        collocations=False,
        min_word_length=3,
        repeat=False,
        relative_scaling=.2,
        font_path="/usr/share/fonts/google-noto/NotoSans-Medium.ttf", max_words=20)

wc.generate(text)

wc_svg = wc.to_svg(embed_font=True)
f = open(output,"w+")
f.write(wc_svg)
f.close()

