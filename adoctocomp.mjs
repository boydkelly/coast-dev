// adoc2dcomp.mjs
import { createRequire } from 'module';
import asciidoctor from '@asciidoctor/core';
import fs from 'fs';
import path from 'path';
import glob from 'glob';

const require = createRequire(import.meta.url);
const asciidoctorHtml5s = require('asciidoctor-html5s');

// Create an Asciidoctor processor
const processor = asciidoctor();

// Register the html5s backend
asciidoctorHtml5s.register(); // Register without passing the processor

// Get the input and output directories from command line arguments
const [, , inputDir, outputDir] = process.argv;

// Check if both arguments are provided
if (!inputDir || !outputDir) {
  console.error('Usage: node adoc2dcomp.js <input-directory> <output-directory>');
  process.exit(1);
}

// Ensure the output directory exists
fs.mkdirSync(outputDir, { recursive: true });

// Get all .adoc files in the input directory and its subdirectories
const sourceFiles = glob.sync(`${inputDir}/**/*.adoc`);

if (sourceFiles.length === 0) {
  console.error('No .adoc files found in the input directory:', inputDir);
  process.exit(1);
}

// Create an Asciidoctor processor
// Function to capitalize the first letter of the filename (excluding the extension)
const capitalizeFirstLetter = (filename) => {
  const extPart = path.extname(filename);
  const namePart = path.basename(filename, extPart);
  return filename.charAt(0).toUpperCase() + namePart.slice(1) + extPart;
};
// Convert each .adoc file
sourceFiles.forEach((file) => {
  if (fs.statSync(file).isFile()) {
    // Ensure it's a file
    const relativePath = path.relative(inputDir, file); // Get the relative path from the input directory
    const titleCasedFilename = capitalizeFirstLetter(relativePath); // Capitalize the first letter of the filename
    const outputPath = path.join(outputDir, titleCasedFilename.replace(/\.adoc$/, '.svelte')); // Replace .adoc with .html
    // Ensure the output directory exists
    fs.mkdirSync(path.dirname(outputPath), { recursive: true });

    // Read the file content
    const content = fs.readFileSync(file, 'utf8');

    // Convert the content to HTML
    const html = processor.convert(content, {
      backend: 'html5s',
      standalone: false,
      safe: 'unsafe',
      attributes: {
        showtitle: true,
        includedir: '_include/',
        imagesdir: '/images/',
        'allow-uri-read': '',
        'skip-front-matter': true,
        icons: 'font'
      }
    });

    // Write the converted HTML to the output directory
    fs.writeFileSync(outputPath, html);

    console.log(`Converted: ${file} -> ${outputPath}`);
  }
});

console.log('Conversion complete!');
