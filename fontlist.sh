#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

[[ -z $1 ]] && exit
image=$1

./fontlist.py "$image"
echo "$image"
magick "$image" -resize 1200x675 -quality 75 -sampling-factor 4:2:0 -strip "${image%.svg}".x.jpg
magick "$image" -resize 1200x630 -quality 75 -sampling-factor 4:2:0 -strip "${image%.svg}".og.png
