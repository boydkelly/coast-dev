#!/usr/bin/bash
# keep words with contractions
# replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
# translate to lower;
# Remove plurals (all words that end in w.)
awk -F "\t" '{ print $1 }' <./proverbs.tsv |
  sed -r 's/(^.*)(w$)/\1/' |
  sed 's/[[:punct:]]//g' |
  sed 's/[[:space:]]\+/\n/g' |
  tr "Ɔ,Ɛ,Ɲ,Ŋ" "ɔ,ɛ,ɲ,ŋ" |
  tr "[:upper:]" "[:lower:]" |
  sort >dyu.list.tmp

# plurals
#> dyu.lower-list.tmp
sort -u dyu.list.tmp >dyu.prvb-word-list.txt
sort dyu.list.tmp | uniq -c | sort -n >dyu.prvb-word-frequency.txt
wc -l dyu.prvb-word-list.txt && rm *.tmp
