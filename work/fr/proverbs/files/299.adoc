:author: Inconnu
:date: 2020-12-27
:description: Sababu man dɔgɔ / La cause n’est jamais petite
:sectnums:
:sectnumlevels: 2
:notitle:
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
|Sababu ko i kana to di ale ma, i kana ji di ale ma, nka i kana ɲina ale kɔ.|La cause dit ne pas lui donner à manger, ni a à boire, mais de ne pas l’oublier.
|Fɛn saba furura fɛn saba ma, ka fɛn saba woro : sigi ka fagantanya woro, tagama ka sɛgɛ woro, wɔsiji ka sɔrɔ woro.|Trois se sont mariés à trois, pour mettre au monde trois : l’oisiveté a mis au monde la pauvreté, la marche a mis au monde la fatigue, la sueur a mis au monde le gain.
|Sagajigi ta kunbiri bɛɛ tɛ siralɔnbariya ye.|Toutes les fois que le bélier baisse la tête ne veut pas dire qu’il ne sait pas la route.
|Sama tɛ bonya kongo ma.|L’éléphant n’est jamais trop grand pour la forêt.
|Sanfinin bɛɛ ji tɛ na.|Tous les ciels nuageux ne donnent pas de la pluie.
|Ni san ɲagamina, kun tɛ karojate ra tugun.|Quand on a perdu le fil des années, il ne sert plus à rien de compter les mois.
|Ni san ɲagamina, kun tɛ karojate ra tugun.|Quand on a perdu le fil des années, il ne sert plus à rien de compter les mois.
|Sani i ye fitinan mana fiyentɔ ye, o turu kɛ a ta sɔsɔ ra, o bena a mako ɲa.|Au lieu d’allumer une lampe à huile pour un aveugle, mets cette huile dans son haricot, c’est mieux pour lui.
|Sani i ye jɛgɛjalan kɛ ka su fifa, i tun ye o kɛ nanji ye k’a di a ɲanaman ma.|Au lieu de souffler le mort avec le poisson séché, il aurait fallu lui en faire un bouillon de son vivant.
|Saninfugula min be kɛnɛbagatɔ kun na, banabagatɔ le be o ye.|Les bien portants ont des couronnes en or, que seuls les malades voient.
|Saraka be bɔ lon min a tɛ mina o lon.|Le sacrifice n’est pas exaucé le même jour où on l’offre.
|Ka san tan kɛ faligwɛn na, hali n’i ma falikan mɛn, i be sirafaran caaman lɔn.|Si tu fais dix ans derrière les ânes, même si tu ne connais la langue des ânes, tu sauras beaucoup de chemins.
|Ɲɔ sannin, ani i yɛrɛ ta ɲɔ sɛnɛnin, o ɲɔ fla diya tɛ kelen ye.|Le mil acheté et le mil cultivé soi-même n’ont pas le même goût.
|Ɲɔ sannin, ani i yɛrɛ ta ɲɔ sɛnɛnin, o ɲɔ fla diya tɛ kelen ye.|Le mil acheté et le mil cultivé soi-même n’ont pas le même goût.
|Sandigi ta foyi tɛ sisɛdenin ta maɲumankokan na.|L’épervier s’en fout des cris de détresse du poussin.
|Ka segi i cɛkɔrɔ ma o tɛ kojugu ye, nka a be mɔgɔ lɔnbaga caya.|Retourner chez ton ancien mari, n’est pas grave, mais ça fait que tu seras connue de beaucoup trop de gens.
|Sagafaga tɛ wurufaga sa, sogo bɛɛ n’a domubaga lo.|Tuer le mouton, n’empêche pas de tuer le chien ; chaque viande a son consommateur.
|Ka i sen don ji ra, ni o be mɔgɔ kɛ bozo ye, ni ne ka n ta fla sama k’a bɔ, n’ tɛ kɛ i yɛrɛ ta ye wa ?|Si mettre mon pied dans l’eau fera de moi un pêcheur, j’enlève donc mes deux pieds pour être libre.
|Sen kelen tɛ sira bɔ.|Un seul pied ne peut tracer un chemin.
|Sisɛ da ka dɔgɔ burufiyɛ ma.|La bouche de la poule est trop petite pour jouer du cor.
|Sisɛ da ma ɲi murusankokuma ra.|Le poulet ne doit pas parler dans une affaire d’achat de couteau.
|Sisɛba tɛ fɛnjugu yɛrɛgɛ k’a kɛ a den kɔrɔ.|La mère poule n’éparpille rien de mauvais devant ses poussins.
|Sibankɔnɔ tɛ laadikan mɛn.|Oiseau qui cherche la mort n’écoute pas de conseils.
|Sisɔrɔ le ka gwɛlɛn ka tɛmɛ siyɔrɔsɔrɔ kan.|Avoir la vie est plus difficile que d’avoir un endroit pour vivre.
|Sigi ka di banba ye, nka a kukala tɛ sɔn.|Le crocodile aimerait bien s’asseoir, mais sa queue refuse.
|Siradarakɔlɔn, n’i tagatɔ ma min a ra, i sekɔtɔ be min a ra.|Puits au bord du chemin, si tu n’y bois pas en allant, tu y boiras au retour.
|Ni so be i ben, i tɛ a toro ye.|Quand le cheval va te faire tomber, tu ne vois pas ses oreilles.
|Sogomuso kɔnɔman, donsocɛ muso kɔnɔman, dɔ na kɛ dɔ ta nanji ye.|La femme enceinte de la proie, la femme enceinte du chasseur, l’un sera sauce pour l’autre.
|Solikabɔ fla tɛ se ka kɛ sɔgɔmada kelen na.|On ne peut se lever tôt deux fois dans la même matinée.
|====
