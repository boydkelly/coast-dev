:author: Inconnu
:date: 2020-12-27
:description: Tiɲɛn tɛ fla ye / La vérité n’est pas deux.
:sectnums:
:sectnumlevels: 2
:notitle:
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
|Tiɲɛfɔbaga ma man di.|L’homme qui dit la vérité n’est pas aimé.
|Kuma fɔ koɲuman ni tiɲɛ tɛ kelen ye.|Parole bien dite, n’est pas égale à vérité.
|Hali ni faninya ka san tan kɛ tagama ra, tiɲɛ ta sɔgɔmada kelen tagama be kun a ra.|Même si le mensonge fait dix ans de marche, une seule matinée de marche de la vérité le rattrape.
|Tiɲɛkuma be mɔgɔ dɔ kɔnɔ, nka a fɔda t’a fɛ.|Quelqu’un peut avoir une parole de vérité mais il n’a pas la bouche pour le dire.
|Tiɲɛn be mɔgɔ ɲa wulen, nka a t’a ci.|La vérité rougit les yeux, mais elle ne les casse pas.
|O b’a fɔ tɔri ma yɔrɔ min ko « Bagayɔgɔ », o ye a buguyɔrɔ ye.|Le moment où on peut (remercier) le crapaud et lui dire : « Bakayoko » c’est le moment où il se couche.
|Tɔri firikojugu b’a bla suma ra.|A force de trop jeter le crapaud, on le met à l’ombre.
|Ni mɔgɔ ma sa, ko bɛɛ juru b’i ra.|Tant qu’on n’est pas mort, tout peut arriver.
|Ni nin yiriboro tun tɛ ne tun bena kɔnɔnin nin faga, ni nin yiriboro tun tɛ fana kɔnɔnin tun tɛ sigi yan.|Sans cette branche, j’aurai tué cet oiseau, mais sans cette branche, l’oiseau ne s’assoirait pas là non plus.
|Ni sanji ka i sɔrɔ togo kolon kɔnɔ, a tɛ lɔ tugun.|Si la pluie te trouve sous une cabane délabrée, elle ne s’arrête plus de tomber.
|Ni sanji ka i sɔrɔ togo kolon kɔnɔ, a tɛ lɔ tugun.|Si la pluie te trouve sous une cabane délabrée, elle ne s’arrête plus de tomber.
|Ni sisɛnin ma « Kus !» mɛn, a na « Paraw » mɛn.|Si la poule n’entend pas la voix qui lui demande de quitter, elle entendra le coup de bâton qui lui demande de quitter.
|Ni sunbara ka diya, nɛrɛbɔbaga tɔgɔ tɛ fɔ.|Quand le soumbala est bon, on ne parle pas de celle qui a récolté le néré.
|Ni tasuma ka ba tigɛ a yɛrɛ ma, a be kɛ a fagabaga boro kɔnɔgwan ye.|Si le feu a traversé le fleuve tout seul, celui qui va l’éteindre aura des problèmes.
|Faninyatigɛbaga le b’a fɔ ko a seere be ba kɔ.|C’est le menteur qui dit que son témoin est de l’autre côté du fleuve.
|Nɔnɔ tɛ dɛgɛ tiɲɛ.|Le lait ne gâte pas dégué.
|Ni jɛgɛdenin ka fɛn o fɛn fɔ banba ta ko ra, a fɔbaga le k’a fɔ.|Tout ce que le petit poisson dit à propos du crocodile, il le dit en tant qu’un vrai témoin.
|Ni tuganin kɔnna ɲɔfiyɛbagaw ɲa, a bena dɔ kɛ sigi ra.|Si la tourterelle arrive avant les vanneuses il attendra longtemps.
|Ɲafɛn tiɲɛ, tiɲɛfɛn fana tɛ ɲa.|Ce qui est destiné à reussir ne se gâte pas, ce qui est destiné à être gâté, ne réussit pas.
|Ɲamanfirisununkun be yi, nka balemafirisununkun tɛ yi.|Il y des endroits pour jeter des ordures, mais il y en a pas pour jeter des frères.
|Ɲa be ko dɔ ye, da tɛ o fɔ, tulo be ko dɔ mɛn, da tɛ o fɔ.|Les yeux voient certaines choses que la bouche ne dit pas, l’oreille entend certaines choses que la bouche ne dit pas.
|Ɲa jiginta tɛ sunɔgɔ.|Des yeux sans espoir ne dorment point.
|Ɲa tɛ doni ta, nka a be doni gwiriman lɔn.|L’œil ne porte pas de bagage, mais il sait reconnaître un bagage lourd.
|Ɲanafin be fali faga.|L’ennui tue l’âne.
|Ɲin gwɛra, nka basi b’a jukɔrɔ.|La dent est blanche, mais il y a du sang (rouge) en bas.
|Ɲinan be tokalama ben, nka tɛ se k’a lawuri.|La souris fait tomber l’ustensile mais il ne peut pas le relever.
|N’i ka ɲinan jalaki, i ye sunbara fana jalaki.|Si tu accuse la souris, il faut aussi accuser le soumbala.
|Fɛn fɔlɔ le ye fɛn fɔlɔ ye, ni min bɔra o kɔ, a be fɔ ko dɔwɛrɛ bɔra.|La première chose est vraiment la première, Ce qui vient après, on dit : « un autre ».
|Ɲɔ be don ji ra lon min, a tɛ kɛ dɔrɔ ye o lon.|Le mil ne devient pas du dolo, le même jour où on le met dans l’eau.
|Ɲɔ be ɲɔbugu, nka takan le b’a ra.|Il y a du mil au village du mil, mais il y a une condition pour le prendre.
|Ɲɔgɔn bamusodomu le be subagaya diya.|Se manger les mères les uns des autres rend la sorcellerie intéressante.
|Warasogo gwaniman le ka di.|La viande d’un fauve est bonne quand c’est chaud.
|Sa be siran, a fagaba be siran.|Le serpent a peur, celui qui le tue a aussi peur.
|Ni sa kunkolo tigɛra, a tɔ kɛra jurukisɛ ye.|Quand la tête du serpent est coupée, il n’est plus qu’un corde.
|N’i ka sa ye ka bere min to i boro i b’a faga ni o ye.|Le bâton que tu as en main au moment où tu vois le serpent, c’est avec celui là que tu le tues.
|Sa ɲa ka misɛn, nka boro tɛ su a ra.|Le serpent a des petits yeux, mais on ne peut y mettre le doigt (en signe de mépris).
|====
