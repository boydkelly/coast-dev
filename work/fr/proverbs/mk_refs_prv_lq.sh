#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

# [[ -z "$1" ]] && {
#   echo "You need to specify a file from nolinks"
#   exit 1
# }

function validate {
  #csvclean -v -t $1
  awk 'BEGIN{FS=","} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
  #delete missing field 1
  sed -i /^,/d $1
  #remove any defs with discontined verbs for now
  sed -i /[...]/d $1
}

files=files
all="/home/bkelly/dev/jula/dyu-xdxf/build/dyu-uri.csv"
stop="stop.dyu.txt"

validate $all
cat ${all} | sed -e "$(sed 's:.*:s/^&,.*$//i:' $stop)" | sed 's/[ \t]*$//' | sed /^$/d >input.csv

file=./proverbs-lq.md
echo Processing $file
while IFS="," read -r word id; do
  sed -i -r -e "/^   >|^:.*/! s/( )($word)(\s)/ \[\[\2\]\] /i" $file
done <input.csv

file=./proverbs.md
echo Processing $file
while IFS="," read -r word id; do
  sed -i -r -e "/^> - |^:.*/! s/( )($word)(\s)/ #\2 /i" $file
done <input.csv

rm input.csv

#git commit -a -m $0
#&& git push
cp proverbs-lq.md ~/notes/logseq/lexique/pages/proverbs-lq.md
cp proverbs.md ~/notes/obsidian/dioula/proverbs/proverbs.md
