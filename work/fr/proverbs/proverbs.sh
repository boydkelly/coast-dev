#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
#

# for x in ls echo yq; do
#   type -P $x >/dev/null 2>&1 || {
#     echo >&2 "${x} not installed.  Aborting."
#     exit 1
#   }
# done

#first make json file from adoc files (and tsv by product)
project=proverbs
for x in tmp tsv md yml json; do export "${x}=${project}.$x"; done

while IFS= read -r -d '' file; do
  cat "$file" |
    sed -e '/^|/!d' | sed -e '/^|=/d' |
    sed -r -e 's/^\||\|$//' |
    sed -e 's/|/\t/' |
    sed -e '/^$/d' >>"$tsv.~"

done < <(find ./files -name "*.adoc" -print0)

sort -u -t $'\t' -k 2 "$tsv.~" >"$tsv"
sort -t $'\t' -k 2 "$tsv.~" | jq -R -s -c 'split("\n") | map(split("\t")) | map({"dyu": .[0], "fr": .[1]})' >"$json"

rm ./*.~

#standard markdown and obsidian
echo "# Proverbes" >proverbs.md
echo "" >>proverbs.md
#logseq
echo "- # Proverbes" >proverbs-lq.md
echo "" >>proverbs.md
#adoc
echo "= Proverbes" >proverbs.adoc
echo "" >>proverbs.adoc

yq -r '.[] | "> " + .dyu + "\n> - " + .fr + "\n"' proverbs.json >>proverbs.md
yq -r '.[] | " - > " + .dyu + "\n   > - " + .fr + "\n"' proverbs.json >>proverbs-lq.md
yq -r '.[] | .dyu + "\n:: " + .fr + "\n"' proverbs.json >>proverbs.adoc

yq proverbs.json -oy >proverbs.yml

cp -v proverbs.tsv ~/dev/jula/dyu-xdxf/src/
./mk_refs_prv_lq.sh
./mk_wordlist2.sh
