= SÉANCE 10 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-05-06
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:description: Cours de dioula séance 10
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LÉÇON 10 : KALANSEN TANNAN

== L’USAGE DE « TUN » POUR MARQUER L’IMPARFAIT OU L’UNE DE SES DERIVES

« Tun » placé avant un auxiliaire (« bɛ », « tɛ », « ka », « ma », « ye…ye ») traduit d’une certaine façon l’imparfait ou l’une de ses dérivés en Dioula de Côte d’Ivoire.

Ci-dessus les exemples de phrases construites avec « tun »

=== Cas 1 : dans les différents contextes du verbe « être »

.Exemple : contexte de la présentation
[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|San timinin, n tun ye kelly ya lakolifa ye|L’année dernière, j’étais le Professeur de Coulibali
|Fɔlɔfɔlɔ, a tun ye sɛnɛnkɛla ye|Avant, il était cultivateur
|Kalo timinin, Youssouf tun ye n sigiɲɔgɔn ye|Le mois dernier, Youssouf était mon voisin
|Mahamane tun ye Alidou tericɛ ye|Mahamane était l’ami de Alidou
|Birahima tun ye saga feerela ye Bouake|Birahima était vendeur de mouton à Bouaké
|N tun tɛ Madou ya sofɛricɛ ye|Je n’étais pas le chauffeur de Madou
|Coulibali tun tɛ Youssouf tericɛ ye|Coulibaly n’était pas l’ami de Youssouf
|Kalo timinin, Kadi tun yɛ Moussa muso ye fɔlɔ|Le mois dernier, Kadi n’était pas encore la femme de Moussa
|San timinin, a facɛ tun tɛ minisiri ye|L’année dernière, son père n’était pas Ministre
|Cɛ nin tun tɛ an baarakɛɲɔgɔn ye|Ce monsieur n’était pas notre collegue/Collaborateur
|===

.Exemple : contexte de la localisation
[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Lɔgɔkun timinin, n tun bɛ Abidjan|La semaine dernière, j’étais à Abidjan
|Fɔlɔfɔlɔ, a tun bɛ Jamanan wɛrɛ/gbɛrɛ|Avant, il était/vivait dans un autre pays
|Kunu sufɛ, Youssouf tun bɛ so kɔnɔ|Hier nuit, Youssouf était à la maison
|Bi sɔgɔma, Mahamane tun bɛ à ya baarakɛyɔɔrɔ la|Mahamane était à son lieu de travail
|Bi terefɛ, Birahima tun bɛ misiri la|Birahima était à la mosquée
|Kunu, n tun tɛ so kɔnɔ|Hier, je n’étais pas à la maison
|San timinin, Coulibali tun tɛ Kɔdiwari|L’année dernière, Coulibaly n’était pas en Côte d’Ivoire
|Kalo timinin, Kadi tun tɛ ni a cɛ ye|Le mois dernier, Kadi n’était pas avec son mari
|San timinin, fen nin tun tɛ yan|L’année dernière, cette chose n’était pas ici
|Bi, Moussa tun tɛ baarakɛyɔɔrɔ la|Aujourd’hui, Moussa n’était pas au travail
|===

.Exemple : contexte de la qualification, des attributs
[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|San timinin, n tun ka bon|L’année dernière, j’étais gros
|Fɔlɔfɔlɔ, Mariam tun ka jugu|Avant, Mariam était méchante
|Den nin tun ka surun|Cet enfant était court
|Mahamane tun ka nyi ni bɛɛ ye|Mahamane était gentil avec tout le monde
|Birahima tun cɛ ka nyi|Birahima était beau
|Fɔlɔfɔlɔ, a tun ma bon|Avant, il n’était pas gros
|San timinin, Coulibali tun ma jugu|L’année dernière, Coulibaly n’était pas méchant
|Kalo timinin, mangorow tun ma di|Le mois dernier, les mangues n’étaient pas douces
|San timinin, fen nin tun ma dɔgɔ|L’année dernière, cette chose n’était pas petite
|Kunu wulafɛ, Moussa tun ma kɛnɛ|Hier soir, Moussa n’était pas malade
|===

=== Cas 2 : dans les différents contextes du verbe « avoir »

.Exemple : Posséder un bien matériels, disposer d’une influence sur ce qu’on possède
[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|San timinin, wari tun bɛ Moussa bolo|L’année dernière, Moussa avait de l’argent
|Mobili tun bɛ a facɛ bolo|Son père avait une voiture
|Fɔlɔfɔlɔ, den tun bɛ cɛ nin bolo|Avant, cet homme avait des enfants
|San timinin, foyi tun tɛ Mahamane bolo|L’année dernière, Mahamane n’avait rien
|Salon, mobili tun tɛ n ya lakolifa bolo|L’année dernière, mon prof n’avait pas de voiture
|Fɔlɔfɔlɔ, lu tun tɛ a bolo|Avant, il n’avait pas de concession (maison)
|===

.Exemple : Possessions inaliénables, expressions de sentiments, de sensation….
[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Kunu, kɔngɔ tun bɛ cɛ nin na|Hier, cet homme avait faim
|San timinin, banan jugu tun bɛ Moussa la|L’année dernière, Moussa avait une grave maladie
|Kunu sufɛ, funteni tun bɛ Mahamane denmuso la|Hier nuit, la fille de Mahamane avait chaud
|San timinin, teri tun tɛ Moussa la|L’année dernière, Moussa n’avait pas d’ami
|Foyi tun tɛ a dencɛ la|Son fils n’avait rien
|===

=== Cas 3 : « tun » placé avant l’auxiliaire de « l’Habituel »

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Fɔlɔfɔlɔ, Coulibali tun bɛ dɔrɔ min|Avant, Coulibaly buvait de l’alcool (boisson alcoolisée)
|San timinin, Ali tun bɛ maro sɛnɛn|L’année dernière, Ali cultivait du riz
|Faransi, Youssouf tun bɛ lɛsogo domun|En France, Youssouf mangeait la viande de porc
|A teri tun tɛ mɔgɔ fo|Son ami ne saluait pas les gens/personne
|Bakari tun tɛ sanni kɛ yan|Bakari ne faisait pas ses achats ici
|Salon, Youssouf tun tɛ baara kɛ Abidjan|L’année dernière, Youssouf ne travaillait pas àAbidjan
|===

=== Cas 4 : « tun » placé avant l’auxiliaire du « progressif »

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Coulibali tun bɛ dɔrɔ min na|Avant, Coulibaly était en train de boire de l’alcool
|Ali tun bɛ maro sɛnɛn na|Ali était en train de cultiver du riz
|Youssouf tun bɛ lɛsogo domun na|Youssouf était en train de manger du porc
|A teri tun tɛ ji min na|Son ami n’était pas en train de boire de l’eau
|Bakari tun tɛ sanni kɛ la|Bakari n’était pas en train de faire des achats
|===

=== Cas 5 : « tun » placé avant l’auxiliaire du « l’éventuel »

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Coulibali tun bena dɔrɔ min…|Coulibaly allait boire de l’alcool…
|Ali tun bena maro sɛnɛn…|Ali allait cultiver du riz…
|Youssouf tun bena lɛsogo domun…|Youssouf allait manger du porc…
|A teri tun tɛna ji min…|Son ami n’allait pas boire de l’eau…
|Bakari tun tɛna sanni kɛ yan…|Bakari n’allait pas faire des achats ici…
|===

=== Cas 6 : « tun » placé avant l’auxiliaire de l’accompli « passé »

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Kunu, n tun ka samara san Ali fɛ|Hier j’avais acheté des chaussures chez Ali
|Penda tun ka a terimuso blasira|Penda avait accompagné son amie
|N muso tun ka baara sɔrɔ|Ma femme avait obtenu du travail
|A teri tun bori la|Son ami avait fui
|Bakari tun taga la Bouaké|Bakari était allé à Bouaké
|Kun lakolifa tun ma baranda san|Hier, le Prof n’avait pas acheté de banane
|===
