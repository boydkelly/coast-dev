= SÉANCE 14 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-06-23
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LÉÇON 14 : KALANSEN TAN NI NAANINAN

== REVISION DES SÉANCES SUR LES COMPARATIFS

=== EXERCICE

✓ Répondre à ces questions

. Sisɛ ni sisɛfan, jɔn ne ka kɔrɔ ni jɔn ye ?
. Ile nyana/hakili la, baganw la jumɛn ne ka cogi ?
. Dɔrɔ ni lemuruji la, jumɛn ne ka timin ?
. Saman ka bon katimin misi kan wa ?
. Yamoussoukro ni Bouaké bonya ye kelen ye wa ?
. Darɛsalam ni Ahunyansu cɛnya ye kelen ye wa ?
. Abidjan ni Bouaké la, dugu jumɛn ne ka bon ?
. Bouaké kin jumɛn ne cɛ ka nyi katimi tɔw kan ?
. Funteni bɛ Bouaké katimi korhogo kan wa?
. Nɛnɛ bɛ Faransi katimi Kanada kan wa?

== REVISION PHRASES COMPLEXES EN DIOULA

✓ Répondez à ces questions

. I bɛ mun kɛ lon bɛ sôgoma kasɔrɔ ka baara damina?
. N ka kan ka mun kɛ jango Coulibali bɛ sɔn ka coca cola min ?
. Muna adamaden dɔw tɛ seli (lanin tɛ Allako la) ?
. Mun kosɔn (muna) i ma sigi Abidjan ka baara kɛ?
. Yanni i bɛ julakan kalan ni ne ye, i tun bɛ julakan kalan mini ?
. I tun b’a fɛ ka n wele tɛnɛn lon, muna i ma n wele tugun ?
. Ile hakili la, muna bananbagatɔ dɔw tɛ taga dɔtɔrɔso la ?
. Tɔnbatigiw ka kan ka mun kɛ janko dɔ bɛ bɔ fangantanya la ?
. Adamaden ka kan ka mun kɛ ni a b’a fɛ ka si sɔrɔ ?
. Ni wari t’i bolo, i te se ka foyi kɛ (tunyan wa wuya) ?

== LE DISCOURS REPORTE EN DIOULA

[abstract]
Les discours reportés sont des discours qui sont rapportés par des personnes autres que celui
(ceux) qui les a (ont) produites. En dioula de Côte d’Ivoire, ces phrases se présentent sous les formes suivantes :

Examples

. Madu ko : « sini, n bena samara san » (discours non reporté : DNR)
. Madu ko a bena samara san sini (discours reporté : DR)
. Merete ko : « n dencɛ ma kɛnɛ » ! (DNR)
. Merete ko a dencɛ ma kɛnɛ (DR)
. Ali ko: « an ya lakɔlifa bɛ sikɛrɛti min » (DNR)
. Ali ko o ya lakɔlifa bɛ sikɛrɛti min (DR)
. Aminata ko: « Madu, i tɛna bɔ bi wa? » (DNR)
. Aminata ka Madu nyiniga n’a tɛna bɔ bi (DR)

=== EXERCICE

Mettre ces phrases à la forme directe et non reporté

. Coulibali ko: Youssouf, an bena kalan kɛ lon jumɛn?
. Youssouf ko: an ka kan ka taga Bouaké
. Alimata ko : kunu n tun ma kɛnɛ
. A dɔgɔcɛ ko: sanji ben na Abidjan bi
. Limamu ko: silamanw, a ka kan ka seli so kɔnɔ sabu Coronavirus bɛ jamanan kɔnɔ
. Birahima ko: denminsew ma kan ka o bangebagaw dɔgɔya
. Penda ko: su ko la, n tɛna se ka nan bi
. Amidou ko : nyina, n bena mobili san
. Siriki ko: sufɛ yala ma nyi, an tɛna bɔ sufɛ
. Adama ko : n ka bamanankan kalan Bamako
