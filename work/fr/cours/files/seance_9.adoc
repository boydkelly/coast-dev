= SÉANCE 9 COURS DE DIOULA 
:author: Youssouf DIARASSOUBA 
:date: 2020-10-19
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LECON 9 : KALANSEN KƆNƆNTƆNAN

== REVISION DES COURS PRECEDENTS

Cette partie est consacrée à l’ensemble des points abordés lors des séances d’apprentissage précédentes.

=== EXERCICE DE TRADUCTION

. Qui est en train d’apprendre le Dioula ?
. Combien d’employés parlent Anglais ici ?
. Lequel des apprenants s’appelle Coulibali ?
. Où sont les jouets de Moussa ?
. Y’a-t-il des ressortissants de Bouaké dans cette Cour ?
. Quel est le nom de famille de ton ami ?
. Coulibali et Youssouf ont-ils des amis à Korhogo ?
. Tu seras à Abidjan la semaine prochaine ?
. Allez-vous travailler samedi prochain ?
. Que vas-tu faire cette nuit ?
. Combien coûte un kilogramme de tomate ?
. L’être humain mange combien de fois par jour ?
. Combien de semaine y’a-t-il dans le mois ?
. Combien de personnes travaillent à Bassam ?
. J’irai à la plage demain avec mes amis
. De nos jours (an ya tere la), les enfants ne respectent pas les personnes âgées
. Chaque jour, il boit de l’alcool avec ses collègues de service ?
. Il se rendra à Bouake jeudi avec tous ses enfants
. Ce monsieur est le frère de ma femme, il travaille à Orange
. Je ne suis pas français mais mes frères vivent là-bas.

== COURS DU JOUR : L’ACCOMPLI EN DIOULA (LE PASSE)

Nous débutons cette nouvelle leçon par un dialogue introductif

=== DIALOGUE INTRODUCTIF

[horizontal]
Coulibali:: I ni wula n badenmancɛ
Youssouf:: N ba, i ni wula
Coulibali:: Hɛrɛ bɛ ?
Youssouf:: Hɛrɛ, dɔ di wula fɛ
Coulibali:: Juguman tɛ, wuladafoli le
Youssouf:: Ah, i ni ce foli la. I bɛ taga la mini ?
Coulibali:: N bɛ taga la n tericɛ fɛ. Ile do, I bena bɔ bi wa?
Youssouf:: ɔn-ɔn, n tɛna bɔ bi, n muso ma kɛnɛ
Coulibali:: Allah ye nɔgɔya kɛ
Youssouf:: Amina. I bɔ la mini ten ?
Coulibali:: N bɔ la n kɔrɔcɛ fɛ
Youssouf:: A ka kɛnɛ wa?
Coulibali:: ɔn-hɔn, a ka kɛnɛ kosɔbɛ
Youssouf:: An bɛ
Coulibali:: An bɛ Youssouf

Les deux phrases en gras sont des constructions au passé c’est-à-dire qu’elles renvoient aux actions déjà accomplies.
L’accompli en Dioula de Côte d’Ivoire décrit les faits qui se sont déjà réalisés. 
Il se construit selon la structure suivante : Sujet+ ka/ma + objet + verbe. 
Il convient de souligner que cette structure s’applique aux phrases transitives.

=== Exemples :

Phrases affirmatives

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Kunu, n ka samara san Ali fɛ|Hier j’ai acheté des chaussures chez Ali
|Madu ka dlɔki do|Madu a porté une chemise
|Penda ka a terimuso blasira|Penda a accompagné son amie
|N muso ka baara sɔrɔ|Ma femme a obtenu du travail
|Kalanfa ka kalandenw ladi|Le prof a prodigué des conseils aux élève
|Mahamane ka sani kɛ|Mahamane a fait des achats
|Alidou ka saga faga seri lon|Alidou a tué un mouton le jour de la fête.
|===

Phrases negatives

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Kunu n ma domuni kɛ|Hier, je n’ai pas mangé
|Bi, n ma bɔ|Aujourd’hui, je ne suis pas sorti.
|Penda ma kalan kɛ|Penda n’a pas étudié
|Lakolifa ma baranda san|Le professeur n’a pas acheter de banane
|N muso ma baara sɔrɔ|Ma femme n’a pas eu de travail
|Bi sɔgɔma, n ma daraka dun|Ce matin, je n’ai pas pris le petit dejeuner
|Kunasini, Ali ma baara kɛ|Avant-hier, Ali n’a pas travaillé
|Lɔgɔkun timinin, maa si ma nan|La semaine dernière, personne n’est venue
|Muso nin ma n fo|Cette femme ne m’a pas salué
|===

« Ka » représente l’auxiliaire de l’accompli et « ma » est sa forme négative. 
En ce qui concerne les phrases intransitives, une modification de la structure est nécessaire.
En effet, dans ce cas de figure, l’auxiliaire est placé après le verbe.

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Musa muso seli la|La femme de Moussa a prié
|N bori la|J’ai fui (Couru)
|Ali sigi la misiri kɔnɔ|Ali s’est assis dans la mosquée
|Moussa kuman na a denw fɛ|Moussa a parlé à ses enfants
|Mɔgɔ bɛɛ taga (taa) la|Tout le monde est parti
|Siriki teriw nan na|Les amis de Siriki sont venus (arrivés)
|Cɛ nin facɛ fatu la|Le père de cet homme est décédé
|Awa den bange la tarata|L’enfant de Awa est né Mardi
|Domuni mɔn na|Le repas est prêt
|Bɔrɔ fa la|Le sac est rempli (plein)
|===

L’auxiliaire « ka » se transforme en « la » ou « na » selon la terminaison du verbe dans les formes positives uniquement.
La même structure (Sujet + ka/ma+ objet + verbe) est conservée pour les constructions négatives.

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
|Musa muso ma seli.|La femme de Moussa n’a pas prié.
|N ma bori.|Je n’ai pas fui (Couru).
|Ali ma sigi misiri kɔnɔ.|Ali ne s’est pas assis dans la mosquée.
|Moussa ma kuman a denw fɛ.|Moussa n’a pas parlé à ses enfants.
|Mɔgɔ bɛɛ ma taga (taa).|Tout le monde n’est pas parti.
|Siriki teriw ma nan.|Les amis de Siriki sont venus (arrivés).
|Cɛ nin facɛ ma fatu.|Le père de cet homme est décédé.
|Awa den ma bange tarata.|L’enfant de Awa est né Mardi.
|Domuni ma mɔn.|Le repas n’est pas prêt.
|Bɔrɔ ma fa.|Le sac n'est pas rempli.
|====

=== EXERCICE DE TRADUCTION

Du Français au Dioula

. Penda a mangé du pain et des omelettes ce matin
. J’ai appris le dioula
. Les frères de Moussa ont parlé anglais
. Le professeur a acheté un sac de riz
. L’amie de mon frère aîné est venue
. La chaussure de Mariam est gâtée
. Siriki est parti hier soir
. J’ai acheté du riz à la boutique de Bourahima
. Qui a vendu ses tomates à ma fille ?
. La mangue est tombée

Du Dioula au Français

. Kunu, n ka domuni kɛ Musa fɛ
. N ma saga feere bi.
. Siriki ka mobili bori kunu.
. An taga la lɔgɔfɛ la lɔgɔkun timinin.
. O bɔ la kɛnɛ ma.
. Musa bamuso kuman na a fɛ.
. Jɔn ne ka baranda nin san ?
. I ka mun bla sibon kɔnɔ ?
. A tericɛ k'a wele kunu nka a ma a ta.
. A se la Abidjan wa ?

=== EXERCICE A FAIRE POUR LA PROCHAINE SÉANCE

* Construire au minimum 10 phrases au passé (5 affirmatives et 5 négatives) en utilisant les verbes transitifs 
* Construire au minimum 10 phrases au passé (5 affirmatives et 5 négatives) en utilisant des verbes intransitifs
