= FANITIGI 
:author: Youssouf DIARRASSOUBA
:date: 2021-09-05
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

== BARO

[abstract]
=== FANITIGI

Sanikɛla ni Fanitigi be baro kɛ

[horizontal]
sanikɛla:: Fanitigi i ni sɔgɔma.
fanitigi:: Nse, hɛrɛ sila wa?
sanikɛla:: Tɔɔrɔ si tɛ n na, i ni lɔgɔ.
fanitigi:: Nba, gbɛrɛ yan, na o flɛ flɛ. Fani ɲumanw bɛ yan dɛ. tɔgɔmanw yɛrɛ. Fani bɔnin kuraw lo.
sanikɛla:: ɛɛɛ tɔgɔmanw wa? cɛn na, an bena gbara (gbɛrɛ) i la dɛ sabu an nana fanisanko kosɔn.
fanitigi:: Ayiwa, a danse. Faniw flɛ nin ye: n sen i sen, sisɛba n’a denw, n cɛ bɛ se ko la, kami, flɛriba, a na ja i la, o n’a ɲɔgɔnna caman.(et plusieurs autres, etc)  (ceke be kɔnɔ ja)
sanikɛla:: « N muso bɛ se ko la », o bɛ i fɛ wa, Fanitigi?
fanitigi:: Eee, n tericɛ, i bɛ yɛlɛko kɛ dɛ.(tu fais rire) ɔn-hɔn, a bɛ yan kɛ.
sanikɛla:: Ile tɔgɔ bɛ di?
fanitigi:: Ne tɔgɔ ye Youssouf, n jamu ye DIARRA. N bɛ fani feere lɔgɔ la yan. mɔgɔw bɛ nan fani san n fɛ yan, o b’a tigɛ ka taga a feere. (il est grossiste)
sanikɛla:: Fanitigi i bena fani caman di n ma bi dɛ.
fanitigi::  An bena bɛn.
sanikɛla:: N bena fani caman san i fɛ. « N sen i sen » o jigi ka na. « Sisɛba n’a denw» na n’o ye, « Flɛriba » , o di. 
« n furucɛ bɛ se ko la », o fara a kan.
fanitigi:: ɛheee n tericɛ, ile tɔgɔ bɛ di? i nan na i nakun na. I nakan ka di.
sanikɛla:: Ne tɔgɔ ko sanikɛla.
fanitigi:: Sanikɛla! Piyɛsi joli joli walima kɔnpile joli joli ? n k’a lɔn ko ile bena muso kura furu.
sanikɛla:: Eee Yousouf, o tɛ kɛnɛ ma kuma ye (pas des paroles de dehors), nka i ma fili dɛ. (tu ne tes pas trompé) 
N ko, n ni n maminamuso yɛrɛ le (c'est avec ma financé même) nana. 
sanikɛla:: Ayiwa jarabi! an bɛ piyɛsi piyɛsi le ta walima an bɛ kɔnpile kɔnpile le ta ?
maminamuso:: Eee, n kanuɲɔgɔn, a kɛ nɔgɔman ye, (fais ce qui est simple) kɔnpile kɔnpile.
sanikɛla:: Ayiwa fanitigi ! i k’a mɛn wa ? Tagafe saba saba, sigiyɔrɔman looru. 
fanitigi:: Eee n ya sanikɛla bɛrɛ, n bɛ kɔnpile kelen feere waga seegifootnote:[5Fx (1000x8)=40000F] nka n bɛ waga kelen kelenfootnote:[5Fx (1000x1)=5000F] bɔ i ye, sabu I ka caman san. Waga woronfila sigiyɔrɔman looru, o bɛ bɛn waga bi saba ni looru ma. 
sanikɛla:: Ale lo eee, i bɛ jate lɔn dɛ. Wari filɛ. Hɔn wari la. Ala ye lɔgɔ diya.
fanitigi:: Amiina, ayiwa i ni ce. Ala ye an bɛn kelen ma. k’an bɛn siɲɛ wɛrɛ. N b’i somɔgɔw fo.
sanikɛla:: O bena a mɛn.

== VOCABULAIRE 

.Vocabulaire
[width="100%",cols="2",frame="topbot",opts="none",stripes="even"]
|====
a|
Ala:: Dieu
Fanta:: Fanta
Jaabi:: Diaby
Jaabidennin:: petit enfant Diaby
a barika:: rabaissez le prix
a:: ça
ale:: ça, lui
amiina:: amen
an:: nous
aw:: vous
ayiwa:: bon
bi:: aujourd’hui
bɔ:: enlever
bɔ:: sortir, enlever
bɔnin:: sorti
bɛ:: aux.
bɛ:: avoir
bɛ:: est
bɛ:: être
bɛn:: entendre
bɛn:: égal
bɛn:: rencontrer, s’entendre
caman:: beaucoup
cɛ:: homme, mari
cɛn:: vérité
dalakuma:: parole
danse:: bonne arrivée
denmisɛnnin:: tout petit
denw:: enfants, poussins
dheee:: rires
di:: bon, bonne
di:: comment ?
diya:: rendre agréable, bon
dɛ:: particule d’assertion
eee !:: Eee !
eee Kulubalicɛ !:: Eee, homme Coulibali !
fani:: pagne
fanisanko:: achat de pagne
fanitigi:: vendeur(se) de pagnes
faniw:: pagnes
fara:: ajouter
feere:: vendre
fili:: perdre, égarer, tromper
flɛ:: voilà, regarder
flɛ:: voir
flɛriba:: grande fleur
fo:: saluer
furu:: épouser, marier
furucɛ:: époux
fɛ:: chez
fɛ:: postposition
gbara:: approcher
gbɛrɛ:: approcher
hɔn:: tiens
a|
hɛrɛ:: paix
i ni ce:: merci
i:: toi
i:: toi, tu
ile:: toi
ja:: rendre sec, sécher
ja:: dur
jamu:: nom de famille
jate:: calcul, compte
jigi:: faire descendre
joli:: combien
juguman:: méchant
ka:: aux.
ka:: être
ka:: pour
kami:: pintade
kan:: dessus, sur
kan:: voix
kelen kelen:: un par un
kelen:: un
ko:: affaire, chose
ko:: que, dire
kosɔn:: à cause de
kura:: nouvelle
kuraw:: nouveaux
kɔnpilɛ:: complet
kɛ:: faire
kɛ:: particule d’assertion
k’a:: l’as
k’an bɛn:: au revoir
la:: au
la:: à
la:: dans
la:: postposition
le:: c’est
lo:: c’est
looru:: cinq
lɔgɔ:: marché
lɔn:: connaitre, savoir
ma:: à
ma:: nég. Passé
maminamuso:: fiancée
matigi:: seigneur
muso:: épouse, femme
mɔgɔw:: gens
mɛn:: comprendre, entendre
n:: je, moi
na:: dans
na:: marque du futur
na:: venir
nakan:: voix de ton arrivée
nakun:: but, objectif d’une visite
nana:: venir+acc
ne:: moi
ni:: avec
a|
ni:: et
nka:: mais
nse:: réponse d’une femme à une salutation
nunnu:: ceux-ci
nɔgɔman:: facile, simple
o n’a ɲɔgɔnna caman:: etc.
o:: ceci, cela, ils, ça
piyɛsi:: pièce
sa:: mourir, priver, empêcher
saba saba:: trois, trois
saba:: trois
sabu:: parce que
san:: acheter
sannikɛla:: acheteur
se:: pouvoir
seegi seegi:: huit huit
sen:: pied
si:: aucun
sigiyɔrɔman looru:: fois 5
sigiyɔrɔman:: fois
sila:: passer la nuit+acc
sisɛba:: mère poule
siɲɛ:: fois
somɔgɔw:: parents
sɔbɛ:: sérieux
sɔgɔma:: matin
ta:: prendre
taa:: aller
tagafe:: pagne
tanin:: petite de, chérie
tericɛ:: ami
tigɛ:: couper, prendre en gros
toron:: amusement, jeu
tɔgɔ:: nom
tɔgɔmanw:: qui portent des noms
tɔɔrɔ:: mal, souffrance
tɛ:: n’est pas, neg
u:: ils, les
wa:: est-ce que ?
waa:: mille
waga bi saba:: cent cinquante mille
waga wolonfla =35000
waga:: mille
walima:: ou bien
wari:: argent
wɛrɛ:: autre
yan:: ici
ye:: postposition
ye:: pour
yɛlɛko:: affaire pour rire, comédie
yɛrɛ:: même
ɔn-hɔn:: oui
ɛɛɛ:: interjection
ɲumanw:: beaux
ɲɔgɔnna:: semblable
|====

== GRAMMAIRE

=== Tigi = propriétaire, le chef, le maître, le seigneur.

[width="100%",cols="4",frame="topbot",options="none",stripes="even"]
|===
|Mobilitigi|Sariyatigi|Nɛgɛsotigi|Kantigi
|Sotigi|Mɔgɔtigi|Burutigi|Kɛlɛtigi
|Yiritigi|Lutigi|Jotigi|musotigi
|Bontigi|Dugutigi|Marifatigi|dentigi
|Jitigi|Kuntigi|Fanitigi|donintigi
|===

.Traductions
[%collapsible]
====
mobilitigi:: propriétaire de voiture
sariyatigi:: juge
nɛgɛsotigi:: propriétaire de vélo
kantigi:: celui qui tien parole
sotigi::  chef de la maison
mɔgɔtigi:: célébrité
burutigi:: panetier
kɛlɛtigi:: chef de l'armée; guerrier principal
yiritigi:: ? 
lutigi:: chef de la cours
jotigi:: quelqu'un d'honnête
musotigi:: homme marié, (contraire cɛ gbana)
bontigi:: propriétaire de maison
dugutigi:: chef de village
marifatigi:: homme armé
dentigi:: quelqu'un avec un enfant
jitigi::  chef des eaux; sodeci
kuntigi:: chef leader guide
fanitigi:: vendeur de pagnes
donintigi:: chef de marchandises
fangatigi:: homme fort 
====

=== « S’approcher de… » = « gbɛrɛ… la »
 
An bɛ gbɛrɛ mɔgɔ ɲuman na.
An tɛ gbɛrɛ mɔgɔ jugu la.
Den bɛ gbɛrɛ a bamuso la


