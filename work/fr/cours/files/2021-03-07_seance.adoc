= SÉANCE 2021-03-07
:author: Youssouf DIARRASSOUBA
:date: 2021-03-07
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== SÉANCE DE REVISION

== TEXT: Garange

Ali ye garange ye.
A bɛ sigi lɔgɔfɛba la ka samaraw kalan, a bɛ bɔɔrɔw fana kalan.
A bɛ gbolo baara kɛ.
A bɛ gbolo tigɛ ka samara kuraw ladinɛ.
A bɛ sɛbɛdenw tugu.
A bɛ baganw fana karan.
Ali bɛ suluyew fana ladinɛ.
A bɛ se samara ladinɛ na.
A bɛ samara kɔrɔw fana kɛ kuramanw ye.
Ali bɛ mɔgɔkɔrɔba samaraw feere.
A bɛ denmisɛntaw fana ladinɛ ka o feere.
Mɔgɔ caman bɛ nan Ali fɛ ka samara san.
A ya samaraw sɔngɔ ka di.
A bɛ wari caman sɔrɔ.


== Vocabulaire

.Vocabulaire
[width="100%",cols="3",frame="topbot",optons="none",stripes="even"]
|====
a|
Ali:: Ali
a:: il
baara:: travail
baganw:: amulettes, talismans
baro:: dialogue
bɔɔrɔw:: sacs
bɛ:: aux.
caman:: beaucoup
denmisɛnnin taw:: pour les tous petits
di:: bon
fana:: aussi, également
feere:: vendre
fɛ:: chez
garange:: coordonnier
a|
gbolo:: peau
ka:: aux. Devant adjectif
ka:: pour
kalan:: coudre
kalansen:: leçon
kura:: nouvelle, neuf, neuve
kuramanw:: nouveaux
kɔrɔw:: vieux, usés
kɛ:: faire
la:: au
ladinɛ:: fabriquer, reparer, reparation
lɔgɔfyɛba:: grand marché
mɔgɔ:: personne
mɔgɔkɔrɔba:: personne âgée
a|
na:: dans
na:: venir
samaraw:: chaussures
seegi:: huit
seeginan:: huitième
sigi:: s’asseoir
sɔngɔ:: prix
sɔrɔ:: trouver
sɛbɛdenw:: gris-gris
tigɛ tigɛ:: couper en plusieurs morceaux
tugu:: fermer, coudre
wari:: argent
ye:: être
ye:: postposition
|====

=== Exercise

Ɲiningali ninugu jaabi

. Garange ya baara ye mun ye ?
. I ka garange dɔ lon Bouaké walima san Pedro wa?
. Samara sugu jumɛn ka di ile ye ?
. Ali bɛ samara sugu jumɛn ladinɛ ?
. Ali bɛ nafa sɔrɔ samaraladinɛ la wa ?
. I b’i ya samaraw labɛn garangew fɛ waliman I b’o labɛnnin san samarafeerelaw fɛ ?
. Samara min labɛn na bolo la ani min labɛn na nin masini ye, jumɛn le ka di ile ye ?
. Fɔlɔfɔlɔ, garange tun ye siyako ye. Ile nyana, bi kow bɛ o cogo kɔrɔ la wa ?

== Point de Grammaire : 

L’insistance et la mise en relief : « yɛrɛ », « fana », « le »

* yɛrɛ 

. Ne yɛrɛ le ka nin labɛn, mɔgɔ gbɛrɛ tɛ
. Ali kɔrɔcɛ yɛrɛ le nan na yan kunu, a baden gbɛrɛ ma nan
. Jenbe tɛ a yɛrɛ fɔ, mɔgɔ le bɛ jenbe fɔ
. Ne yɛrɛ bena a wele sini sɔgɔman joonan
. Ne yɛrɛ ma kɛnɛ bi

* fana 

. Ale dɔrɔn tɛ se ka baara nin kɛ, ne fana be se
. Ale kelen ma furu, anugu fana furu la
. Coulibali dɔrɔn ma don, anugu fana don na bon kɔnɔ
. Cɛ nin dɔrɔn tɛ baara kɛcogo lon, ne fana k’a lɔn
. Muso nin fana ma nan furusiriyɔrɔ la karilon

* le 

. Ne tɛ, ale le ka bon nɔgɔ
. Fɔlɔfɔlɔ, Garange dɔrɔn le tun bɛ gbolo baara
. Ale le bɛ mɔgɔw dɛmɛn ka baji tɛgɛ
. Siriki le ka nin kɛ, mɔgɔ gbɛrɛ tɛ
. Coulibali ma foyi kɛ bi, Youssouf le ka ko bɛɛ kɛ

=== Baarakɛta : 

Propose au moins trois (03) phrases pour chacun des contextes ci-dessous
