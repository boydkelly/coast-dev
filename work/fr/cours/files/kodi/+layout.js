import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core';
const processor = Processor();

// this is needed to get the asciidoc attributes.
export const load = async () => {
  const file = await import(`./+page.adoc?raw` /* @vite-ignore */);
  if (file.default) {
    const attributes = processor.load(file.default).getAttributes();
    // const doctitle = processor.load(file.default).getDocumentTitle()
    return {
      attributes
    };
  }
  error(404, 'Not found');
};
