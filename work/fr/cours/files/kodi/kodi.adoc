= Kódì 2022 (notes)
:author: Université d'Abijdan
:description: Kodi notes pour l'apprentissage de la langue dioula
:date: 2020-05-07
:doctype: book
:sectnums:
:partnums:
:part-signifier: UNITÉ
:chapter-signifier: Leçon
:sectids: 1
:sectlinks: 1
:note-caption: Remarque
:sectnumlevels: 4
:!toc: 
:toclevels: 1
:icons: font
:tags: jula, dioula, dyula, kodi, leçon, lesson, abidjan, "côte d'ivoire"
:categories: Jula
:page-aliases: fr@julakan:cours/kodi/index.adoc
:lang: fr
:docinfo: shared
:sub: kodi/
:includedir:
include::{includedir}locale/attributes.adoc[]

include::{includedir}anki-link.adoc[]

.INTRODUCTION
[abstract]
Qu'est-ce que le Dioula ?
À cette question d'aspect si simple, les réponses apparaissent, selon les auteurs, et selon les locuteurs eux-mêmes, si divergentes, en introduction à ce cours d'expliquer notre position sur ce point.

include::{includedir}{sub}unite-1.adoc[]

include::{includedir}{sub}unite-2.adoc[]

include::{includedir}{sub}unite-3.adoc[]

include::{includedir}{sub}unite-4.adoc[]

include::{includedir}{sub}unite-5.adoc[]

include::{includedir}{sub}unite-6.adoc[]

include::{includedir}{sub}unite-7.adoc[]

include::{includedir}{sub}unite-8.adoc[]

include::{includedir}{sub}unite-9.adoc[]

