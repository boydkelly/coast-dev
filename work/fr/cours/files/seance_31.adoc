= SÉANCE 31 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-10-10
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉCON 31 : KALANSEN BI SABANAN NI KELENNAN
EXPLICATIONS DE QUELQUES NOTIONS GRAMMATICALES

== EMPLOI DU SUFFIXE « NIN » DANS LE CONTEXTE DE LA DESCRIPTION DES ETATS

L’emploi de la structure grammaticale «Sujet+ verbe-(suffixe)nin+bɛ/tɛ » exprime généralement l’état de quelque chose ou de quelqu’un à l’instant présent.
Avec cette construction, on obtient des formes adjectivales en dioula.

* Coulibali siginin bɛ sigilan kan
* Youssouf lɔnin be Ali ya butiki kɔfɛ
* Barikon nin fanin bɛ
* I muso sawanin be bi, mun ko bɛ yi ?
* N sɛgɛnin bɛ kojugu
* Cɛ nin nisɔdiyanin (sawanin) tɛ kabini kunu
* Bakari diminin be à muso kɔrɔ
* A ya masirifenw nɔrɔnin bɛ danan na
* Ji gbanin bɛ kojugu
* I dencɛ nyagbannin bɛ
* Muso nin jamanin bɛ

Des spécialistes parlent de suffixe « resultatif ». Vous pouvez egalement entendre des locuteurs dioulaphones dire:

* Coulibali siginin lo/le sigilan kan
* Youssouf lɔnin lo/le Ali ya butiki kɔfɛ
* I muso sawanin lo/le bi

== QUELQUES EXPLICATIONS CONCERNANT LES SUFFIXES « BAGA » ET « LA »

a. « Baga » (Celui qui…. : agent temporaire, agent de circonstance)
b. « la » (Celui qui…. : agent dont c’est le metier)

== LE SUFFIXE « TƆ » ET SES EMPLOIS

=== CONTEXTE 1

Il peut avoir pour equivalent le suffixe « tɔ » dans le cas ou « le fait d’etre pourvu ou de contenir queluqe chose» n’est pas volontaire ou provoqué

* Banabagatɔ (malade: quelqu’un qui porte une maladie)
* Fiyentɔ (quelqu’un qui est porteur de la cessité)
* Dɛsɛbagatɔ (quelqu’un qui est «pouvu»/victime du manque)
* Kunantɔ (lepreux, victime de la lepre ou qui porte la lepre)
* Dangatɔ (maudit, qui porte la malediction)
* Fatɔ (fou; qui porte la folie/ Victime de folie)

=== CONTEXTE 2

Participe présent introduisant une forme de simultaneité, de concomitance

* Coulibali bɔtɔ bɛn na ni Moussa ye (En sortant, Coulibali a rencontré Moussa)
* An nantɔ ka mangoro san sira la (Nous avons acheté des mangues en venant)
* Moussa kumantɔ ka mɔgɔw nɛnin (En parlant, Moussa a insulté les gens)
* Youssouf bena i ya telefɔni san, a dontɔ so kɔnɔ (Youssouf va acheter ton téléphone en rentrant à la maison)

== EXERCICE DE TRADUCTION

[cols="2*"]
|===
a|
Francais/Dioula

. En revenant de l’école, il a rencontré son frère
. Certains fous ne mangent pas la nourriture avariée
. Le professeur de  est couché sur le lit
. Les personnes malades n’aiment pas la nourriture sucré
. Les demunis ne mangent pas trois fois par jour
. Cet enfant est maudit, il ne respecte personne

a|
.Traduction:
[%collapsible]
====
. a kɔsigitɔ ka bɔ lakoliso la, a bɛn na ni a ya badɛncɛ ye
. fatɔ dow tɛ domuni torinin domu
. Coulibali ya karamɔgɔ lanin bɛ lafɛn kan
. domuni sukarontan man di banabagatɔw ye
. dɛsɛbagatɔw tɛ domuni kɛ siɲɛ saba tere kɔnɔ
. den nin ye dangatɔ ye, a tɛ mɔgɔ si bonya
====
|===

[cols="2*"]
|===
a|
Dioula/Francais

. A nantɔ ka nanfenw san lɔgɔfɛ la
. Youssouf bɛ yeli kɛ, fiyentɔ tɛ
. Dangatɔ tɛ mɔgɔkɔrɔbaw bonya
. Muso nin bɛ tiga kɔgɔman feere MADOU ya butiki kɛrɛfɛ
. Den nin ye mɔgo hakiliman ye, a bɛ a kumancogo kɔrɔsi

a|
.Traduction:
[%collapsible]
====
. En venant du marché il a acheté des condiments
. Youssouf est voyant, il n'est pas aveugle
. Les maudits ne respectent pas les vieux
. Cette femme vend des arachides salées à côté de la boutique de Madou
. Cet enfant est intelligent, il pèse ses paroles
====
|===

