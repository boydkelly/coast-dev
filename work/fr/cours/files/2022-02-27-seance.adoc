= 2022-02-27-SEANCE
:author: Youssouf DIARRASSOUBA
:page-author: Youssouf DIARRASSOUBA
:date: 2022-02-27
:page-revdate: 2022-02-27
:description: Séaance d'apprentissage de Jula le 27 février, 2022
:keywords: jula, dioula, leçon, abidjan, 'Ivory Coast', Côte d'ivoire
:category: Julakan 
:page-tags: jula, dioula, leçon, abidjan, 'Ivory Coast', Côte d'ivoire
:lang: fr 
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== 
. W: Un morphème ajouté aux mots pour former le pluriel
. Dɔ (article indéfini signifiant dans bien des cas « un ». +
  Il peut également vouloir dire « certains », « certaines » quand il porte la marque du pluriel)
. Nin (adjectif démonstratif : celui-ci, celui-là, celle-ci, celle-là, ceci…)
. Ninugu (adjectif démonstratif : ces, ceux-ci, ceux-là, celles-ci, celles-là…)
. Min (pronom relatif)
. Tɔgɔ (pronom relatif …en question, …dont on a parlé auparavant…)
== 

== Exemples:

cɛ dɔ:: Un homme
muso tɔgɔ:: La femme en question (La femme dont on a parlé il n’y a pas longtemps)
cɛ dɔw:: Certains hommes
cɛw dɔ:: L’un des hommes
muso nin:: Cette femme
niningu cɛ ka nyi:: Ceux-ci sont beaux/Celles-ci sont belles
muso min:: La femme qui…..
baarakɛla ninugu:: Ces travailleurs

=== Construire cinq phrases avec chacun des articles, pronoms relatifs ou adjectifs démonstratifs étudiés au cours de cette leçon 

cɛ do na nan an ya dugu 
i be se ka julkakan sɛbɛ dɔ sɔrɔ wa?

muso tɔgɔ da ka ki dɛ.
cɛ tɔgɔ, a ka kaso kɛ, 

cɛ dow be kafe min, tɔɔw be te min.

cɛw do ye marfatigi ye.
dencɛw do kɛra fama ye.

dencɛ nin jogo man ɲi.
kalan nin ka nɔgɔ.

mobili ninugu cɛ ka ɲy.

wulu min be kule su fɛ, n bena faga.

== SEANCES D’EXERCICES

=== REPONDRE A CES QUESTIONS

. Mɔgɔ joli bɛ i ya so kɔnɔ?
. Burufeereyɔrɔ bɛ i ya so kɛrɛfɛ wa?
. Ile ye jɔn bamuso ye
. MARIE ye i cɛ jamun ye wa?
. Den jumɛn ye i den ye?
. Jɔn ye I terimuso ye

.Réponses
[%collapsible]
====
. mɔgɔ kelen bɛ n ya so kɔnɔ.
. ɔn ɔn. Burufeeyɔrɔ tɛ yen n y so kɛrɛfɛ.
.
====

=== REMPLISSEZ LES LIGNES VIDES

Exemple :  Muso nin ma nyi, a ka jugu

. Donin nin ma gbilin, a…………………………………………………………………………...

[%collapsible]
====
fɛgɛman be yen
====

. N muso ma jan, a ………………………………………………………………………………………………

[%collapsible]
====
ka surun
====

. Lemuru nin ka kuna, …………………………………………………………………………………………..

[%collapsible]
====
a man di. 
====

. N ya kalanfa ka jugu, a………………………………………………………………………………………….

[%collapsible]
====
ka ɲi
====

. Ali muso cɛ ka nyi, a …………………………………………………………………………………………….

[%collapsible]
====
a ma jugu
====

. Borifen nin ka teli, a……………………………………………………………………………………………..

[%collapsible]
====
ma surun
====

. Musa ya sigilan ka bon, a………………………………………………………………………………………

[%collapsible]
====
ma fitini
====

. Maro nin kisɛ ka misɛn, a…………………………………………………………………………………



=== TRADUISEZ CES PHRASES

. Qui est cette femme?
. La fille qui est dehors, est belle.
. La femme dont je t’ai parlé est Malien.
. Je suis ton voisin, mon nom est Abou.
. Est-il ici ou au marché ?
. Mon professeur n’est pas ici, il est au travail.
. Où es-tu Mariam ?
. Il y a un chien dans ma voiture.
. Il n’y a pas d’argent dans ma poche.
. Y’a-t-il des bagages dans cette voiture ?
. Il n’y a pas de moustique dans la maison d’Ali.
. Il n’y a personne dehors.
. Combien de stylo y’a-t-il sur ta table ?


.Réponses
[%collapsible]
====
. muso nin ye jɔn ye?
. denmuso min be kɛnɛma ka ɲi. 
. muso tɔgɔ be bɔ mali la.
. n ye i ya sigiɲogon ye. n tɔgɔ ko Abu.
. a be yan walima a be lɔgɔfiyɛ?
. N ya lakolifa tɛ yen. A be baarakɛyɔrɔ la.
. I be min Mariam?
. Wulu be  n ya mobili kɔnɔ.
. Wari tɛ n ya posi kɔnɔ
. Doni be yen, mobili nin kɔnɔ?
. Soso tɛ yen Ali ya bon kɔnɔ.
. Mɔgɔ si kɛnɛ ma.
. Biki juman tabali kan?

====

=== Vocabulaire

Joli:: Combien
Soso::  moustique
Mɔgɔ:: personne
Doni:: bagage
Biki:: stylo
Kisɛ:: grain
Misɛn:: petit, fin
Borifen:: véhicule


