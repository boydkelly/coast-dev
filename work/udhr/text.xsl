<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:udhr="http://efele.net/udhr">

  <xsl:output method="text" />

  <xsl:template match="text()">
    <xsl:value-of select="normalize-space()" />
    <xsl:text>&#10;</xsl:text> <!-- Adds a new line after each text node -->
  </xsl:template>

  <xsl:template match="*">
    <xsl:apply-templates />
  </xsl:template>

</xsl:stylesheet>

