<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xpath-default-namespace="http://efele.net/udhr">

  <xsl:output method="html" indent="yes" />
  <xsl:mode on-no-match="shallow-copy" />

  <!-- Root template: Remove <html> and <body> -->
  <xsl:template match="/">
    <xsl:apply-templates select="/*" />
  </xsl:template>

  <xsl:template match="/">
    <script>
            <![CDATA[
  import { OrderedList, ListItem, ImageLoader, InlineLoading } from "carbon-components-svelte";
            ]]>
  </script>

    <!-- Your existing transformations -->
    <xsl:apply-templates />
  </xsl:template>

  <!-- Title -->
  <xsl:template match="title">
    <h1 class="bx--heading bx--productive-heading--06">
      <xsl:value-of select="." />
    </h1>
    <p class="paragraph">
      © The Office of the High Commissioner for Human Rights 1996–2009
    </p>
  </xsl:template>

  <!-- Preamble as a <section> -->
  <xsl:template match="preamble">
    <section class="bx--feature">
      <div class="sect1">
        <h2 class="bx--heading bx--productive-heading--05">
          <xsl:value-of select="title" />
        </h2>
        <div class="sectionbody">
          <div class="imageblock left">
            <div class="content">
              <img src="/images/udhr.svg" alt="Universal Declaration of Human Rights logo">
              </img>
            </div>
          </div>
        </div>
        <xsl:apply-templates select="para" />
      </div>
    </section>
  </xsl:template>

  <!-- Wrap all articles in a single section -->
  <xsl:template match="udhr">
    <xsl:apply-templates select="title" />
    <xsl:apply-templates select="preamble" />
    <section class="">
      <xsl:apply-templates select="article" />
    </section>
  </xsl:template>

  <!-- Articles -->
  <xsl:template match="article">
    <div class="sect1">
      <h2 class="bx--heading bx--productive-heading--04">
        <xsl:value-of select="@number" />. <xsl:value-of select="title" />
      </h2>
      <div class="sectionbody">
        <xsl:apply-templates select="para | orderedlist" />
      </div>
    </div>
  </xsl:template>

  <!-- Paragraphs -->
  <xsl:template match="para">
    <p class="paragraph bx--text-paragraph">
      <xsl:value-of select="." />
    </p>
  </xsl:template>

  <!-- Ordered Lists -->
  <xsl:template match="orderedlist">
    <OrderedList expressive="true">
      <xsl:apply-templates select="listitem" />
    </OrderedList>
  </xsl:template>

  <!-- List Items -->
  <xsl:template match="listitem">
    <ListItem>
      <xsl:value-of select="." />
    </ListItem>
  </xsl:template>

</xsl:stylesheet>

