import fs from 'fs';
import * as cheerio from 'cheerio';
const filePath = './src/routes/fr/docs/lexique-coul/Dioula.svelte'; // Change this to your actual file path
const htmlContent = fs.readFileSync(filePath, 'utf-8');
const $ = cheerio.load(htmlContent);
const lunrIndex = { docs: [] };

const articles = $('article.dl');

articles.each((index, dlEl) => {
  const Text = $(dlEl).find('h4').text().trim();
  const id = $(dlEl).find('h4').attr('id').trim();

  const examples = $(dlEl)
    .find('dl dt')
    .map((i, dtElement) => {
      return $(dtElement).text().trim();
    })
    .get();

  lunrIndex.docs.push({
    Text,
    id,
    examples
  });
});

const lunrIndexJson = JSON.stringify(lunrIndex, null, 2);
fs.writeFileSync('./src/lib/data.json', lunrIndexJson);

console.log('Lunr data created and saved to data.json');
