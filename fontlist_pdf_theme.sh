#!/usr/bin/bash

# Input and output files
FONT_LIST="$HOME/dev/fontlist/fontlist.txt"
THEME_FILE="$HOME/dev/_resources/themes/fontlist-theme.yml"

# Start the YAML theme file
cat <<EOL >"$THEME_FILE"
extends: default
base:
  font-color: #333333
  font-family: Noto_Sans-regular
  font-size: 14
heading:
  h1:
    font-size: 24
  h2:
    font-size: 20
font:
  catalog:
    merge: true # set value to true to merge catalog with theme you're extending
EOL

# Loop through each font in the list and add it to the YAML file
while IFS= read -r fontname; do
  # Extract the font name without the extension
  font_name=$(basename "$fontname" .ttf)

  # Add the font to the catalog
  cat <<EOL >>"$THEME_FILE"
    $font_name:
      normal: $fontname
      italic: $fontname
      bold: $fontname
      bold_italic: $fontname
EOL
done <"$FONT_LIST"

# Start the roles section
echo "role:" >>"$THEME_FILE"

# Loop again to add roles for each font
while IFS= read -r font; do
  # Extract the font name without the extension
  font_name=$(basename "$font" .ttf)
  role_name=${font_name//_/-}

  # Add the role for the font
  cat <<EOL >>"$THEME_FILE"
  $role_name:
    font_family: $font_name
EOL

done <"$FONT_LIST"
