set -o errexit
set -o nounset
set -o pipefail
yq -p=csv -oj money.csv | yq -pj -oj '.[] |= (.id = .numeric)' >money.json
#yq -p=csv money.csv -oj money.csv >money2.json
