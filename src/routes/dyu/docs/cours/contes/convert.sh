#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
#./header.sh "$1"
downdoc "$1" -o - | pandoc -f markdown -t gfm -o "${1%%.adoc}.md"
