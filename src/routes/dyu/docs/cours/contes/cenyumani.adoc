= Cɛnyumani
:author: unknown
:date: 2025-02-24
:keywords: dioula, jula, dyula, contes
:category: Julakan
:tags: Julakan, Language, Contes, Histoires
:description: Dioula conte à propos de Cɛnyumani
:lang: dyu
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

Nin kɛ la dugudɔ la, sunguruni dɔ tun bɛ o dugu la o tun cɛ ka nyi ni
dugu bogotigini bɛɛ ye. A kaba la a ya cɛnya la, a la la a yɛrɛ la. Bɛɛ
bɛ kule ni a tɔgɔ ye. Bogotiginin tɔgɔ ye _Cɛnyumanin_. Ala ni a
cɛnyumaya kosɔn, dugumɔgɔw ma tɔgɔ gbɛrɛ sɔrɔ a la fɔ cɛnyumanin.
Sunguru tɔgɔ tɛ baro kɛ ni dugukamelenw ye fɔ kamelen minw bɛ bɔ dugu
gbɛrɛ la ka nan baro kɛ a fɛ ani olugu le ka di.

Dugukamelen nyuman caman bɔ la Cɛnyuman kɔ furu la nka a ban na o bɛɛ
la. Nin min bɔ la a fɛ furu la, a b’a fɔ a tɛ o fɛ sabu ale bɛ mɔgɔ fɛ
min cɛ ka nyi i n’a fɔ ale yɛrɛ.
Mɔgɔw kuman na bogotiginin fɛ ka dɛsɛ. Hali a tun bɛ si musokɔrɔba min
fɛ o fana kuman na a fɛ ka dɛsɛ a ma sɔn. O ya dugu kɔrɔnnya fan fɛ,
jinaman yɔrɔ dɔ le bɛ yen, mininyaba tun bɛ o tu kɔnɔ.
O miniyaba le ka i yɛrɛma ka i kɛ cɛnyumanba ye ka nan baro kɛ
bogotiginin fɛ. Ah ! Kamelen diya la bogotiginin ye kosɔbɛ. Musokɔrɔba o
ye kolɔnbaga ye, o sɔmin na ka a fɔ : « Nin tɛ adamaden ye, nin ye
jinanyɛlɛman le ye. » _Cɛnyumani_ kɔni kɛ la kamelen fɛ fɔ kamelen nan
na bɔ a kɔ furu la. Ka bogotiginin nyini a ya mɔgɔw fɛ furu la.

Sunguru ya mɔgɔw sɔn na ka bogotiginin di a ma furu la. Fɔlɔ lahada la,
bogotiginin bɛɛ bɛ nyɔgɔn sɔrɔ musokɔrɔbakelen fɛ. O bɛ o ya baara bɛɛ
kɛ yen, o bɛ o ya baro bɛɛ kɛ yen. Cɛnyuman bɛ si musokɔrɔba min fɛ o
sigiya la, bogotiginin dɔgɔmuso dɔ fana tun bɛ musokɔrɔba fɛ, o fana
sigiya la. Kuman bɛɛ fɔ la nka a ko ale tɛ sigi mɔgɔ gbɛrɛ kun ni
kamelen nin tɛ. Bogotiginin di la kamelen ma. Furu kɛnin kɔfɛ, lahada
la, nin kɔnyɔmuso bɛ taga a cɛ fɛ, o bɛ a dɔgɔmusonin dɔ la a kan.

A ni a dɔgɔmuso min tun bɛ musokɔrɔba fɛ ole la la a kan. Musokɔrɔba ka
kilisi kɛ bɛlɛkisɛ den saba kan ka o di dɔgɔmuso ma ka a fɔ ko ni a taga
la kɛ cogoya min na, dɔgɔmuso ye bɛlɛkisɛw ta ka a yɛrɛ dɛmɛ ni o ye.
Bogotiginin ni a dɔgɔmuso taga la furu kɛnin kɔ. O taga la a sɔrɔ cɛ
siyɔrɔ bɛ tuba dɔ le kɔnɔ. O bɛɛ ka sɔrɔ cɛ mɔgɔlaman le. Wagati dɔ nan
na se, cɛ b’a fɛ ka muso fla domu sabu minanya yɛlɛmanin le bɛ. Su ko la
minkɛ, dɔgɔmuso ma sɔn ka sunɔgɔ, a la la ka a yɛrɛ kɛ sunɔgɔbagatɔ ye.

Su se la arabakonjo la tuman min na, cɛ ka a yɛrɛ yɛlɛman ka kɛ
mininyanba ye ka i munumunu ka i ton kɔnyɔnmuso kɛrɛfɛ o tuma na a bɛ
sunɔgɔ la. Dɔgɔmuso ka a kun kɔrɔta ka mininyanba lanin ye. Dɔgɔmuso ka
i makun tehu ! Dugu gbɛ la minkɛ, cɛ taga la baarakɛyɔrɔ la wagati min
na, dɔgɔmuso ko kɔrɔmuso ma ko : « I bɛ cɛ min kun nin tɛ adamaden ye dɛ
! Kunu sufɛ an sunɔgɔnin, cɛ nin yɛlɛman na ka kɛ mininyanba ye i
kɛrɛfɛ. Sani a ye nan an y’an yɛrɛ nyini. »

Kɔrɔmuso ka dɔgɔmusoman lamɛn. O ka o yɛrɛ nyini. Cɛ nanin ka bɔ
baarakɛyɔrɔ la, a ka a sɔrɔ a muso tɛ yen, a musodɔgɔmuso fana tɛ yen. A
ka a lɔn sisan ko o bɔ la a ya ko kala ma, a ka o nɔ mina. A bori la o
kɔ, ka bori o kɔ. Olugu fana bɛ bori la. O ka a sɔrɔ musokɔrɔba ya bɛlɛw
sirinin bɛ dɔgɔmusoman ya fanikun na. Kabini o ka cɛ ye a yɛlɛman na ka
kɛ mininyanba ye, a bɛ wili, a bɛ i yɛrɛ fili, a bɛ wili, a bɛ i yɛrɛ
fili. A surunya na o la tuman min na, kɔnyɔnmuso dɔgɔmuso ka bɛlɛkisɛ
kelen fili a ya bɛlɛkisɛ saba la. O kɛ la tintinba ye mininyan ni o cɛ.
Sani mininyan bɛ o tintinba ta ka a ban, o ka a sɔrɔ o ka caman boli.
Mininyan nan na surunya o la tuguni, o tuman na dɔgɔmuso ka bɛlɛkisɛ
flanan fili, o kɛ la tuba ye ka yiriw meleke nyɔgɔn na o ni nyɔgɔn cɛ.

O ka a sɔrɔ kɔnyɔnmuso ni a dɔgɔmuso ka caman bori tugu sira la.

O to la o cogoya la sisan, fɔ mininyan nan na surunya o la tuguni.
Bɛlɛkisɛ sabanan min tun to la, a ka o fili. O bɛlɛkisɛ kɛ la tasumaba
ye. Mininya nan na lɔ sani a ye ban tasuma faga la ni a daji ye, o ka a
sɔrɔ kɔnyɔnmuso ni a dɔgɔmuso ka caman kɛ sira la.

Baa le bɛ kɔnyɔnmuso ni mininyanba ya yɔrɔ cɛ. O nan na se baa nin da
la. Mininyanba bɛ o kɔfɛ nka mɔgɔ tɛ yen ka o latimi. Kɔnɔ dɔ le bɛ baa
nin fana na. O ye jigifakɔnɔ ye, a bɛ to ka mɔgɔw tigɛ tuman dɔw la. O
ka kɔnɔ ye minkɛ, dɔgɔmuso ko kɔnɔ ma :

__Tigɛ tigɛ baa la kɔnɔnin fin kangalamanjan (2fois) Misi b’an faso baa
la kɔnɔnin fin kangalamanjan Saga b’an faso baa la kɔnɔnin fin
kangalamanjan Tigɛ tigɛ baa la kɔnɔnin fin kangalamanjan__

Kɔnɔnin nan na ka nan kɔrɔmuso ta ka taga o bla baa kɔfɛ ka segi ka nan
dɔgɔmuso kɔ sisan. Dɔgɔmuso ka ya dɔngilinin la tugunin, a ko :

*_Comptine_*

A ka dɔgɔmuso fana tigɛ ka taga bla baa kɔfɛ. Mininyanba nan na sisan.
Tigɛcogo tɛ mininyanba la. Mininyaba fana tɛ dɔnkili nin lɔn. Dɔgɔmuso
le nan na lɔ ko:

*_Comptine_*

Kɔnɔ nan na don mininyan kɔrɔ ka mininya ta. O se la ba cɛman minkɛ,
dɔgɔmuso ko kɔnɔ ma :

***_A bila bila baa la kɔnɔninfin kangalamanjan (****2fois**) Misi t’a
faso baa la kɔnɔninfin kangalamanjan Saga t’a faso baa la kɔnɔninfin
kangalamanjan Sajugu le bɛ baa la kɔnɔninfin kangalamanjan_* *_A bila
bila baa la kɔnɔninfin kangalamanjan_*

Kɔnɔ ka i cɛ ka bɔ mininyaba kɔrɔ ka mininya bla baji la. Kɔnyɔmuso ni a
dɔgɔmuso se la ka bɔsi ka sira sɔrɔ ka taga o ya dugu la. Ole kosɔn muso
i ka kɛ cogo o cogo i kana i nyalawoloma kojugu cɛko la.

« Ni i ma fara nyanawoloma kojugu la cɛko la, i laban bɛ taga bɛn sara
ma i bɛ nimisa min cɛli la. Ni i cɛnya na cogo o cogo danben min bɛ i la
ole ye furu ye. Nka furu fana kɔnɔ, ni i ko i bɛ i nyalawoloman le: N tɛ
nin fɛ, n bɛ nin fɛ. Ni nyalawoloman dansago la i laban bɛ nimisa. »

N ko ta yɔrɔ min na n ko bla yen

=== Baarakɛta : Nyiningali ninugu jaabi

* Nyiningaliw
[arabic]
. Muna bɛɛ bɛ kule ni Cɛnyumani tɔgɔ ye ?
. Cɛnyumani tun b’i nyalawoloma cɛko la wa ?
. Cɛnyumani tun bɛ baro kɛ ni dugukamelenw ye wa ?
. Cɛnyumani furu la mɔgɔ le ma walima jinan ?
. cɛnyumanin cɛ b’a yɛrɛ yɛlɛman wagati jumɛn ?
. Jɔn ne fɔlɔ sɔmin na k’a lɔn ko cɛnyumanin cɛ tɛ adamaden ye ?
. Musokɔrɔba ka fɛɛrɛ jumɛn yira cɛnyumanin n’a dɔgɔmuso la ?
. Mun ne ka cɛnyumanin n’a dɔgɔmuso bɔ bolo la ?
. Danbe jumɛn bɛ muso bɛɛ la ?
. Ile hakili la, musow jo le k’o nyalawoloman cɛko la wa ?

*jaabiliw*

==== . Bɛɛ bɛ kule ni Cɛnyumani tɔgɔ ye sabu a cɛ ka nyi kojugu. .
Ɔn-hɔn, Cɛnyumani tun b’i nyalawoloma cɛko la. . Ɔn-ɔn, Cɛnyumani tun tɛ
baro kɛ ni dugukamelenw ye. . Cɛnyumani furu la jinan le ma. .
cɛnyumanin cɛ b’a yɛrɛ yɛlɛman lon bɛɛ sufɛ. . Cɛnyuman dɔgɔmuso fɔlɔ le
sɔmin na k’a lɔn ko cɛnyumanin cɛ tɛ adamaden ye. . Musokɔrɔba ka kilisi
kɛ bɛlɛkisɛ den saba kan k’o di dɔgɔmuso ma ka a fɔ ko ni a taga la kɛ
cogoya min na, dɔgɔmuso ye bɛlɛkisɛw ta k’a yɛrɛ dɛmɛ n’o ye. .
Musokɔrɔba ya bɛlɛkisɛw le ka cɛnyumanin n’a dɔgɔmuso bɔ bolo la. . Muso
bɛɛ danbe ye furu ye. . Ne hakili la, muso ka kan k’a nyalawoloman cɛko
la nka a ma nyi a b’a danatimin.
