#!/usr/bin/bash
# keep words with contractions
# replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
while IFS= read -r -d '' file
do
  echo $file
#  epub2txt $file > ${file%.*}.txt
## replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, delete single letter words except aeinou, sort, remove dups
#  epub2txt --raw $file > ${file%.*}.upper.tmp
  cat $file > ${file%.*}.upper.tmp
## remove words with contractions
# replace all characters that are not alphanumeric wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
# epub2txt  --raw $file | sed -e "s/[^[:alpha:]]/ /g; s/[[:space:]]/\n/g" | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sed -e '/.*w$/d' | sort | uniq > ${file%.*}.no-contractions.dict.tmp
done <   <(find . -name "*.adoc" -print0)

# make this one word per line and then nuke thousands of french greek and hebrew words
# words that contain apostrophe after second character, apostropy at begining of word
# This is stuff where there is no real need to check.  It can't be jula.
# Remove all words that end in w.  Jula plurals not needed.
cat *.upper.tmp | sed -e "s/[^[:alpha:]’]/ /g; s/[[:space:]]/\n/g" \
  | sed -e "/[àâéèêëìîïòôûüçÀÂÌÏÎÉÊËÖÔœæqQxX]/d" \
  | sort | uniq > dyu.upper-list.tmp
 # && 
 # rm *.upper.tmp

# create lists of french, english latin words in file
for x in fr_FR; do
  echo $x
  hunspell -G -d $x dyu.upper-list.tmp | sort | uniq > remove.$x.word-list.tmp
done 

# remove french english and latin words
#cat remove.*.word-list.tmp | sort |uniq > all.word-list.txt && rm remove.*.word-list.tmp
comm -23 dyu.upper-list.tmp <(cat remove.*.word-list.tmp) > dyu.no-foreign.tmp && rm remove.*.word-list.tmp

# make all lower case, remove jula plurals, remove single letter words with exceptions; remove doubles and sort
#cat dyu.no-foreign.tmp  | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sed -e "/^[^aeinou]$/d" | sort | uniq > dyu.lower.tmp
cat dyu.no-foreign.tmp  | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sort | uniq > dyu.lower.tmp
# && rm dyu.no-foreign.tmp

#cat dyu.clean.tmp dyu.addback.txt | sort | uniq > dyu.word-list.txt
# # | sed -e '/.*[ʽʼ]$/d' \
cat dyu.lower.tmp | sort | uniq > dyu.index-list.txt
cat dyu.index-list.txt | wc -l
