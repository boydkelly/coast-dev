#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

#this just gets the attributes but the latter seems to work for any include partial$
sed -i 's/include:.*attrib.*/include::locale\/attributes.adoc[]/' *.adoc
sed -i 's/include::.*partial\$/include::{includedir}/' *.adoc
