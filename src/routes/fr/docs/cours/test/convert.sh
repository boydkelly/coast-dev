#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
downdoc 2021-01-31_seance.adoc -o downdoc.md
downdoc 2021-01-31_seance.adoc -o - | pandoc -f markdown -t gfm -o gfm.md
downdoc 2021-01-31_seance.adoc -o - | pandoc -f markdown -t commonmark -o commonmark.md
