# SÉANCE 2021-01-31

## COURS DE DIOULA: SÉANCE 2021-01-31 

## DIALOGUE

* **Yousouf**\
i ni sͻgͻma Mariyamu
* **Mariyamu**\
nse, hɛrɛ bɛ?
* **Youssouf**\
hɛrɛ, n b’a fɛ ka i ɲininga ko dͻ la.footnote:[Je voudrais te demander quelque chose], i badenw be jamanan jumɛn na?
* **Mariyamu**\
o bɛɛ be faransi ni n facɛ ye, ile do?
* **Youssouf**\
ne badenw be Isia, Daloa kɛrɛfɛ
* **Mariyamu**\
ne ma Isia ni Daloa lͻn sabu ma dege ka taga yen
* **Youssouf**\
ile ka Kͻdiwari dugu jumɛn le lͻn ?
* **Mariyamu**\
ne ka Duekué, Abijan ni Buake le lͻn
* **Youssouf**\
mun ka i lase.footnote:[conduire, envoyer, guider] o duguw la ?
* **Mariyamu**\
n ya baara le ka n lase dugu saba nin na
* **Youssouf**\
i tun taga la baara jumɛn kɛ o dugu saba la
* **Mariyamu**\
n tun b’a fɛ ka farafina landa.footnote:[valeurs africaines] kͻrͻw faamu ka ɲan
* **Youssouf**\
i bena segi Abijan lon jumɛn?
* **Mariyamu**\
n bena segi Abijan zwekalo la, inʃala.
* **Youssouf**\
an be lon dͻ Mariyamu!
* **Mariyamu**\
an be yusuf !

## PERFECTIONNEMENT

Certaines phrases sont incorrectes d’un point de vue syntaxique, sémantique ou logique.

* Essayez de corriger les imperfections.
  1. N tun ka maro domun sini
  2. A ka taga lͻgͻfɛ la sͻgͻma
  3. I ma baranda domun na
  4. I dencɛ ka kan te nan sini sͻgͻma jonan
  5. Mariyamu be se julakan fͻ
  6. N ka di baranda ye
  7. N ye n facɛ ya bon kͻnͻ
  8. I tͻgͻ ye di Madu walima Musa
  9. Jͻn mobili be kɛnɛ ma
  10. I ya bolo bɛ jan paris la
  11. N kͻngͻ bɛ n na bi, n bena malo ni tigadɛgɛnan domun
  12. A muso ya jugu, a te mͻgͻ boɲan

<details>
<summary>Details</summary>

1. n tun be maro domu sini
2. a tagala lɔgɔfɛ la sɔgɔma
3. i tɛ baranda domu na
4. i dencɛ ka kan ka nan sini sɔgɔma jonan
5. Mariyamu be se ka julakan fɔ
6. baranda ka di n ye
7. n ye n facɛ ka bon kɔnɔ
8. i tɔgɔ ye Madu walima Musa
9. jɔn ya mobili be kɛnɛ ma
10. ?
11. kɔngɔ bɛ n na bi, ne bena malo ni tagadɛgɛnan domu
12. a muso ka jugu, a te mɔgɔ bonya
</details>

### EXPRESSIONS UTILES

* **n ya foli be a ye**\
Recevez mes salutations
* **n ɲan gbanin bɛ bi**\
 Je suis très occupé aujourd’hui
* **i hakili to a la**\
Gardes‐le en mémoire! Veille là‐dessus
* **i jan to à la**\
Prends en soins!
* **i yɛrɛ kͻrͻsi**\
Fais attention à toi !
* **a ka a ya layiri dafa**\
Il a tenu ses promesses
* **i kana jigi n na**\
Ne sois pas déçu de moi!
* **n se ko tɛ**\
Je n’y peux rien !
* **n ya so ɲanafin be n na**\
J’ai la nostalgie de mon village
* **a be sͻrͻ (a nan na)**\
Il est probable que/Il se peut qu’ (il soit venu)
* **a dari n ye**\
Demandes‐lui pardon (de ma part)!
* **n ma ko sͻrͻ à la / A tɛ a kalaman**\
Je ne suis pas au courant

### APPLICATION

Traduire les phrases suivantes:

1. I kana jigi n na, n se ko tɛ
2. I hakili to n denw la, n be taga lͻgͻfɛ la
3. An tena se ka ɲͻgͻn ye bi, n ɲangbanin bɛ
4. I facɛ be mini? a be sͻrͻ a be baarakɛyͻrͻ la
5. I yɛrɛ kͻrͻsi muru la, a da ka di
6. N te a ya wayasi kala man
7. I kana a dari, a jo tɛ
8. N bamuso ɲanafin be n na kojugu
9. N ya mͻntͻrͻ be jͻn bolo? a be sͻrͻ i muso bolo
10. I janto i yɛrɛ la, banan juguw caya la dugubaw la

<details>
<summary>Details</summary>

1. Ne soyez pas déçus de moi, je ne puis rien
2. Veuillez sur les enfants; je vais au marché
3. Nous allons nous voir aujourd’hui, je suis occupé
4. Ou est ton père? Il se peut qu’il soit au lieu de travail
5. Fais attention au couteau, c’est tranchant!
6. Je ne suis pas au courant de son voyage
7. Ne lui demande pas, il n’a pas raison
8. J’ai beaucoup la nostalgie de ma mère
9. Qui a ta montre? Elle est probablement avec ta femme
10. Prenez soin. Il y a beaucoup de mauvaises maladies en ville
</details>

An be sini terefɛ inʃala!
A Demain après‐midi s’il plait à Dieu!

* **bonya**\
respecter, considérer, obéir à…
* **a da ka di**\
c’est tranchant
* **a jo tɛ**\
il n’a pas raison, il a tort ; A jo le : il a raison
