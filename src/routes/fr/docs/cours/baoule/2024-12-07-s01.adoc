= SEANCE 01
:author: Clément N'Goran
:date: 2024-12-07
:keywords: Baoulé
:category: Baoulé
:tags: leçons
:description: SÉANCE 01
:page-author: Clément N'Goran
:page-image: blog
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

[abstract]
====
LIKE KLELƐ CƐN : 07 DEC 2024 +
LIKE SUAN FUƐ : COURS COLLECTIF +
LIKE KLEFUƐ : Clément N’GORAN +
====

== LE SYSTÈME PHONÉTIQUE DU BAOULÉ
À ce niveau, il est question de se familiariser avec l’ensemble des sons qui caractérisent la langue Baoulé.
Nous allons les diviser en consonnes et en deux types de voyelles.

=== Les Consonnes

[width="100%",cols="4",frame="topbot",options="header",stripes="even"]
|===
|API|Orthographe Baoulé|Exemple|Traduction
|b|B|ba|enfant
|c|c (tch)|cɛn|grossir
|d|D|dᴐ|heure
|f|F|Fu|monter
|g|G|gowle|biche
|gb|gb|gbo|cuisine
|j|j (dj)|jᴐ|hamac
|k|K|kalɛ|crédit
|kp|kp|kpɛ|couper
|l|l|La|coucher
|m|m|manzi|machine
|n|n|nanni|bœuf
|ɲ|ny (gn)|nyin|grandir, fermer
|p|p|poyo|variété de banane douce
|r|r|rɛrɛ|échelle
|s|s|se|dire
|t|t|tu|déterrer
|v|v|vi|rein
|j|y|yi|épouse
|z|z|Nza|variété d’igname
|===

=== Les voyelles simples

[width="100%",cols="4",frame="topbot",options="header",stripes="even"]
|===
|api|orthographe baoulé|exemple|traduction
|a|a|ta|planter
|e|e (é)|be|cuire
|ɛ|ɛ|tɛ|mal
|i|i|ti|tête
|o|o|bo|fond
|ᴐ|ᴐ|kᴐ|aller
|u|u|fu|monter
|===

=== Les Voyelles nasales

orthographe

[width="100%",cols="4",frame="topbot",options="header",stripes="even"]
|===
|api|exemple|baoulé|traduction
|i|in|fin|venir de
|a|an|tan|péter
|ἑ|ɛn|fɛn|vider
|ɔ|ɔn|tɔn|préparer
|u|un|wun|voir enflé
|===

EXERCICE D’APPLICATION : LIRE LES MOTS OU EXPRESSIONS SUIVANTS

flua:: grippe
nzue:: rivière
Cɛn klaman:: Ce n'est pas une bonne journée
ᴐntᴐn lɛ::
kpɛma:: couper
Ntie:: il y a
ayinman:: demain
blᴐfuɛ:: blanc
gbekle:: souris
nin kpanngᴐ::
buteli:: boutielle
duvin:: bin
Srɛ kumin:: N'ai pas peur
jue:: poisson
manza:: eau
wunmien:: énergie
Bese:: alors
awun nyɛɛ:: toi maintenant
nzra ma:: étoile
anangaman Nyamien:: éternelle dieus
nyan nyan:: c'est ca?
ba kangan mu:: ne me touche pas
alaje:: sûr
Suin:: porc
talua:: dame
ᴐ yo fɛ::
Klo lɛ:: amour
kᴐkᴐti:: porc
laliɛ:: rêve
aviefuɛ:: voleur

[width="100%",cols="6",frame="topbot",options="header",stripes="even"]
|===
|Formes|||||
|Personne|Sujet|||Complément|
|||élidé|En contexte||En contexte
|1 sing.|Min|m’, n’|min’ kle flua (j’enseigne)|mi|kle mi (montre-moi)
|2e sing.|ɑ, ɛ, wᴐ|w’|ɛ kle i atin (tu lui montres le chemin)|wᴐ|n’ kle wᴐ like (je temontre quelque chose)
|3e sing.|ᴐ, i||ᴐ kle i yalɛ (il/elle le/la fatigue)|i|n’ kle i (je lui montre)
|1e plu.|ye, e|y’|e kle nglɛlɛ|e|ᴐ kle e (il nous montre)
|2e plu.|amun,ame|am’|am’ kle|amu|ᴐ kle amu
|3e plu.|Be|b’|be kle (ils/elles montrent)|be|ᴐ kle be (il leur montre)
|===

== LES PRONOMS PERSONNELS

=== EXERCICE : LIRE LES PHRASES SUIVANTES

[horizontal]
Ɔ seli be kɛ:: il leur a dit
Min wun ti kpa:: je vais bien
Nzue wɛ kun mi:: j'ai soif
Nti man blᴐfuɛ:: je ne suis pas blanc
Amun klᴐ ti klaman:: votre village est beau… ?
Moh ni balɛ::  Bienvenu
N’ wan yo:: je dis que
Anangaman ɲamien klo sran:: L'éternel Dieu aime l'humanité
Klun wi tia kpa:: la méchanceté n'est pas bien
Nguan yo lɛ sa:: Il faut vivre pour voir  / Tout dépend de la vie

