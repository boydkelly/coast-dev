import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core';
const processor = Processor();

export async function load ({ params }) {
  try {
    // Import the .adoc file (if it exists)
    const adocFile = await import(`../${params.slug}.adoc?raw`).catch(() => null);
    let attributes = {};

    if (adocFile?.default) {
      attributes = processor.load(adocFile.default).getAttributes();
    }

    return {
      attributes,
    };
  } catch {
    throw error(404, 'Not found');
  }
};

