=  Ɲiningali
:author: Youssouf DIARRASSOUBA
:date: 2021-07-27
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex:
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

. Mɔgɔ min bɛ adamadenw kalan, o b’a wele di julakan na?
adamadenyalontigi.

. Mɔgɔ min tɛ mɛnni kɛ, o b’a wele di julakan na?

o b'a wele ko tulogwele.

. Mɔgɔ min tɛ kuman, o b’a wele di julakan na?
o b'a wele ko bobo.
. Baara ma di mɔgɔ min ye, o b’a wele di julakan na?
mɔgɔ ma di baara ma di a ye.
salabagatɔ
. Ile nyana, Adamaden bɛ se ka kan joli fɔ?

do be se ka kan tan fɔ.  dow be se ka kaan kelen fɔ.

. Ile nyana, Adamaden bɛ se ka tere joli kɛ jiminbaliya la ?
nga Coulibali bɛ se ka tere kelen kɛ kafeminbaliya la.

n k'a google ɲini.  Adamaden bɛ se ka tere saba kɛ jiminbaliya la.

. Ile bɛ se ka tere joli kɛ kɔngɔ la ?

n be se ka tere fila kɛ kɔngɔ la.

. I bɛ se ka foli kɛ kanyan julakan na wa?

ɔh hɔn.  n be se ka foli kɛ kaɲan julakan na.

. Jakuman bɛɛ bɛ se nyinan minan na wa?

o bɛɛ bɛ se.  o ka ɲɔgɔ ɲinan minan.

. Selilon na, i sigiɲɔgɔnw ka sagasogo (walima domuni) d’i ma wa?

senegal tabalitigi ka cep di n ma.

. Fenw (feerefenw ni domunifenw) sɔngɔ yɛlɛn na Bouake wa ?
n tɛ nanfɛnw sɔngɔ termɛn
n tɛ teremen kɛ

. Ile hakili la, muna fenw sɔngɔ gbɛlɛya la ?
adamadenw tɛgɛ ka gwɛlɛ

. Misisogo kilo kelen bɛ sɔrɔ joli Bouaké sisan ?
misisogo filet kelen bɛ sɔrɔ kɛmɛ segi bouaké sisan.
salon filet tun bɛ sɔrɔ kɛmɛ woronfla.

. Fen jumɛn sɔngɔ ka di (nɔgɔ) katimi fɛn tɔw kan ?
sisɛ sɔgɔ sɔngɔ ka di katimi jɛgɛ kan bouaké
nga jɛgɛ sɔngɔ ka di katimi sisɛ sɔgɔ sɔngɔ san pedro
