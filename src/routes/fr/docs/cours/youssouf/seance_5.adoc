= SÉANCE 5 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-05-15
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

[abstract]
== LECON 5 : KALANSEN LOORUNAN

== REVISION DE LA SÉANCE 4

A travers cette révision, l’apprenant va approfondir ses connaissances concernant les contextes d’emploi du verbe « avoir » en dioula de Côte d’Ivoire.

=== Exercice de traduction

. Coulibali a beaucoup des moutons
. Je n’ai pas de chien à la maison
. L’être humain a combien de pieds ?
. Il y a combien de semaine dans le mois ?
. Mahamane a combien d’enfants ?
. Youssouf n’a pas de voiture
. Sa voiture n’a pas de phare
. Le fils de mon amie a faim
. Est-ce que ton fils a chaud ?
. De quoi as-tu envie/Tu as envie de quoi ?
. Est-ce que vous avez froid ?
. Combien de personnes ont faim ici ?
. Coulibali a sommeil ce matin
. Certaines personnes n’ont pas de cheveux
. Est-ce que le fils de ton professeur à des jouets ?
. Est-ce que le frère de Moussa à un cuisinier ?
. Je n’ai pas d’employé de maison
. La femme de mon ami à un gros chat
. Le fils du cuisinier n’a pas d’ami
. Son enfant à une maladie



[abstract]
== COURS DU JOUR : LES CHIFFRES ET LES NOMBRES
--
Objectif : Maîtriser le système de calcul en Dioula
--

== Kalan lanyini

=== Dialogue introductif

[horizontal]
Coulibali:: sisɛfan den joli bɛ segi kɔnɔ?
Youssouf:: sisɛfan den saba
Coulibali:: joli bɛ sisɛkuru kɔnɔ?
Youssouf:: sisɛfanw ka ca, n ma da lɔn
Coulibali:: sisɛfan den tan
Yusuf:: N tɛ se ka jatebɔ fɔ ka se tan
Coulibali:: N bena i dege

==== Traduction du dialogue

[horizontal]
Coulibali:: Combien y a-t-il d’œufs dans la corbeille ?
Youssouf:: Trois œufs
Coulibali:: combien y en a-t-il dans le poulailler ?
Youssouf:: Il y en a beaucoup, je ne connais pas le nombre
Coulibali:: Dix œufs
Youssouf:: Je ne sais pas compter jusqu’à dix
Coulibali:: Je vais t’apprendre

[width="100%",cols="4",frame="none",options="none",stripes="even"]
|===
|1. un|kelen|6. six|wɔɔrɔ
|2. deux|fila|7. sept|2oronfila
|3. trois|saba|8. huit|seegi
|4. quatre|naani|9. neuf|kɔnɔntɔ
|5. cinq|looru|10. dix|tan
|===

== Les nombres (grands chiffres et sommes d’argent)

Le tableau ci-après permet de générer de façon automatique les chiffres et les nombres même les plus longs pour mieux comprendre le fonctionnement du système de calcul.

.Système de calcul 
[width="100%",cols="12",frame="topbot",options="header",stripes="even"]
|====
3+^|miliyari
3+^|miliyɔn 
3+^|waga 
3+^|kɛmɛ

|kɛmɛ
|Bi 
|ni
|kɛmɛ
|Bi
|ni
|kɛmɛ
|bi
|ni
|kɛmɛ
|bi
|ni 

| | | | | | | | | | | |6
| | | | | | | | | | |4|6
| | | | | | | | | |2|4|6
| | | | | | | | |3|2|4|6
| | | | | | | |5|3|2|4|6
| | | | | | |7|5|3|2|4|6
| | | | | |9|7|5|3|2|4|6
| | | | |8|9|7|5|3|2|4|6
| | | |1|8|9|7|5|3|2|4|6
|7|3|5|1|8|9|7|5|3|2|4|6
|====

.Définitions des termes du tableau
[width="100%",cols="3",frame="topbot",options="none",stripes="even"]
|====
a|
miliyari:: milliard
kɛmɛ:: centaine
miliyɔn:: million 

a|
bi:: dizaine
waga:: mille 
ni:: unité
kɛmɛ:: cent
 
a|
10.:: tan 
20.:: mugan
30.:: bi saba
40.:: bi naani 
|====

[CAUTION]
====
Ne pas traduire deux dizaines par *bi fila** ou encore une dizaine par *bi kelen* ; par contre à partir de 30, le nombre de dizaine qui compose le chiffre est pris en compte. 
====

=== Lisons maintenant les chiffres qui sont dans le tableau :

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
|6
|wɔrɔ

|46
|bi naani ni wɔɔrɔ

|246
|kɛmɛ fila ni bi naani ni wɔɔrɔ

|3246
|waga saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ

|53246
|waga bi looru ni saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ

|753246
|waga kɛmɛ woronfila ni bi looru ni saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ

|9753246
|miliyɔn kɔnɔntɔ ani waga kɛmɛ woronfila ni bi looru ni saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ

|89753246
|miliyɔn bi segi ni kɔnɔntɔ ani waga kɛmɛ woronfila ni bi looru ni saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ

|189753246
|miliyɔn kɛmɛ ni bi segi ni kɔnɔntɔ ani waga kɛmɛ woronfila ni bi looru ni saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ

|735189753246
|miliyari kɛmɛ woronfila ni bi saba ni looru ani miliyɔn kɛmɛ ni bi segi ni kɔnɔntɔ ani waga kɛmɛ woronfila ni bi looru ni saba ani kɛmɛ fila ni bi naani ni wɔɔrɔ
|====

[NOTE]
====
*« ani »* permet de passer d’une grande classe à une autre,les grandes classes étant miliyari,
miliyɔn, waga et kɛmɛ.Pour passer d’un élément à un autre dans la même classe on se sert de *« ni »*.
+
[horizontal]
11:: tan *ni* kelen
12:: tan *ni* fila
105:: kɛmɛ *ni* looru
====

Tous ces chiffres ne concernent que les superficies, les distances... Mais pas les sommes d'argent(en CFA) puisque la base du calcul dans ce cas est 5.
On dira par exemple *tan* pour 50 francs CFA, *mugan* pour 100 francs, *bi saba* pour 150 etc...

=== EXERCICE 1 : Traduire ces chiffres en lettres (d’abord en somme d’argent, ensuite en termes de distance ou de superficie, etc.)

. 2500
. 3500
. 3450
. 4320
. 200 
. 175

.En termes d'argent
[%collapsible]
====
. 2500: kɛmɛ looru
. 3500: kɛmɛ woronfila
. 3450: kɛmɛ wɔɔrɔ ni bi kɔnɔntɔ
. 4320: kɛmɛ segi ni bi wɔɔrɔ ni naani
. 200: bi naani
. 175: bi saba ni looru 1. 2500 kɛmɛ looru
====

.En termes de distance ou de superficie...
[%collapsible]
====
. 2500 waga fila ni kɛmɛ looru
. 3500 waga saba ni kɛmɛ looru
. 3450 waga saba ni kɛmɛ naani ni bi looru
. 4320 waga naani ni kɛmɛ saba ni mugan
. 200 kɛmɛ fila
. 175 kɛmɛ ni bi woronfila ni looru 
====

=== EXERCICE : Traduire ces phrases et tenter de proposer des réponses

. Lemuru layɔrɔ kelen ye joli ye ?
. Baranda kilo kelen ye joli ye ?
. Dɔrɔmɛn joli bɛ i bolo ?
. Joli bɛ muso nin ya saki kɔnɔ ?
. Kafe tasi kelen ye joli ye?
. Malobɔrɔ kilo mugan ni looru bɛ Amadu ya butiki kɔnɔ wa?
. I bɛ baarakɛyɔrɔ la ni mɔgɔ joli ye ?
. Tele joli bɛ lɔgɔkun kɔnɔ ?
. Kalo joli bɛ san kɔnɔ
. Sen joli bɛ Adamaden na?

