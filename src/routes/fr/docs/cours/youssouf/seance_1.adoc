= SÉANCE 1 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-04-28
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== LECON 1 : KALANSEN FƆLƆ

[abstract]
Objectif de la leçon : Savoir écrire et prononcer les mots en Dioula de Côte d’Ivoire

== LETTRES DE L’ALPHABET DIOULA : GRAPHIE ET PHONETIQUE

NOTE: A ce stade, le sens des différentes mots et phrases importe peu. Le plus important est de manifester sa maitrise de la prononciation des lettres de l’alphabet

.Le tableau des lettres 
|===
| Lettre mini|Lettre majuscule|Prononciation|Comme dans|Qui signifie (en français
|a|A|Comme en français|baba|papa
|b|B|Comme en français|baba|papa
|c|C|tch|cɛ|homme
|d|D|Comme en français|daba|daba (houe)
|e|E|Comme (é)|se|pouvoir
|ɛ|Ɛ|Comme (è,ê)|furucɛ|mari, époux
|f|F|Comme en français|fata|gifler
|g|G|Comme en français|saga|mouton
|i|I|Comme en francais|bi|aujourd’hui
|j|J|Comme en Anglais|ji|eau
|k|K|Comme en français|kaba|maïs
|l|L|Comme en français|la|(se) coucher
|m|M|Comme en français|maro|riz
|n|N|Comme en français|nɔnɔ|lait
|ŋ|N|Comme en Anglais|ŋana|brave
|ɲ|ɲ|Comme en francais|ɲɔ|mil
|o|O|Comme en français|bolo|bras
|ɔ|Ɔ|Comme en français|bɔrɔ|sac
|p|P|Comme en français|pan|sauter
|r|R|Comme en français|sira|route
|s|S|Comme en français|soso|moustique
|t|T|Comme en français|toro|oreille
|u|U|Comme en français|ku|igname
|w|W|Comme en français|wara|panthère
|y|Y|Comme en français|yaala|(se) promener
|z|Z|Comme en français|zɔnzɔn|crevette
|===

.LES VOYELLES ORALES ET LEURS CORRESPONDANTES NASALES
[width="100%",cols="2",frame="none",options="header,footer",stripes="even"]
|===
|Voyelles orales|Correspondantes nasales
|i|in
|e|en
|ɛ|ɛn
|a|an
|o|on
|u|un
|ɔ|ɔn
|===

=== EXERCICE 1 : Lire les mots suivants :

[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
|samara|sɛgɛ|fasoden
|denmuso|bɔrɔ|denminsɛn
|cɛ|sanu|sinji
|musoden|jɛgɛ|ɲɛgɛn
|baaraden|nɛnɛ|funteni
|bamanankan|ten(bara)|kɔrɔta
|binkɛnɛ|ben|kanu
|baranda|sɛbɛ|dugalen
|sɔn|adamaden|hakili
|borifen|nɛgɛso|ɲanafin
|====

===  Exercice 2 : Lire les phrases suivantes

. kunu, Ali ka ku san Musa fɛ
. samara bɛ Yusuf sen na
. dereke gbɛman be n facɛ ya saki kɔnɔ
. jɔn ne ka mobili nin boli ?
. lemuru bɔrɔ kelen ye joli ?
. den joli bɛ muso nin fɛ?
. n bamuso taga la lɔgɔfɛ la
. siriki bɛ tɛmɛn bɔnbɔn feere a ya butiki la
. a ye ji bɔn yɔrɔ bɛɛ
. Ala jɔn tɛ dɔrɔ min

== LECON 2 : KALAN SEN FILANAN

=== LA FAMILLE, LES PROCHES ET AMIS (Un clin d’œil aux possessifs)

.Vocabulaire
[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
a|
Grand père:: mamacɛ
Grand-mère:: mamamuso
Père:: facɛ (fa)
Mère:: bamuso (ba)
Oncle paternal:: benɔgɔcɛ
a|
Oncle maternel:: benincɛ
Tante:: tɛnɛnmuso
Sœur:: badenmuso
Grande sœur:: kɔrɔmuso
a|
Petite sœur:: dɔgɔmuso
Frère:: badencɛ
Grand frère:: kɔrɔcɛ
Petit frère:: dɔgɔcɛ
|====

[abstract]
Objectif : Construire des groupes de mots en Dioula de Côte d’Ivoire.
Nous nous appuyons sur le fait que les liens entre différentes entités sont souvent marqués par une forme d’appartenance ou de possession.

.Exemple :
[width="100%",cols="3",frame="none",options="none",stripes="none"]
|===
|Possesseur|Possédé|Glose
|n|facɛ|mon père
|o|tericɛ|leur ami
|i|den|ton enfant
|===

Mais avant d’aller plus loin, il convient de signaler qu’il existe en Dioula deux types de relations possessives.

. La possession aliénable
. La possession inaliénable

La différence entre ces deux types de relations possessives se situe au niveau de l’insertion ou non d’un seul élément (ya).
Il n’est attesté que dans le cadre de la possession aliénable et sépare le possesseur de l’objet possédé.

WARNING: Le ya se trouve typiquement en Dioula de Côte d'ivoire.  Vous trouverez 'ta' et 'ya' aussi dans le Bambara, et Mandingue.

* La possession aliénable

Il s’agit ici des éléments que nous pouvons fabriquer, reconstituer, gérer et influencer tels que voiture (mobili), chaussure (samara), table (tabali), chemise (dereke), etc…

[width="100%",cols="5",frame="topbot",options="header",stripes="even"]
|===
|Possesseur|Marque de possession aliénable|Possédé|| Glose
|N|ya|mobili|n ya mobili|ma voiture
|A|ya|samara|à ya samara|sa chaussure
|Penda|ya|dereke|Coulibali ya dereke|la chemise de Coulibaly
|===

* La possession inaliénable

A la différence de la possession aliénable, la possession inaliénable concerne des liens fraternels, des relations très intimes, la désignation des parties du corps et tous les éléments constituant une partie d’un ensemble.
Dans ce cas de figure, la marque de possession aliénable (ya) n’est pas réalisée.

[width="100%",cols="5",frame="topbot",options="header",stripes="even"]
|===
|Possesseur|Marque de possession aliénable|Possédé||Glose
|n|-|bolo|n bolo|ma main
|a|-|muso|à muso|sa femme
|Coulibali|-|terimuso|Coulibaly terimuso|l’amie de Coulibaly
|===

WARNING: Il faut noter que dans la construction possessive en dioula, la détermination nominale se fait de la droite vers la gauche. Ainsi, nous aurons :

....
a. La chemise de l’ami de kadi
 [1]          [2]     [3]
dereke ya tericɛ kadi <1>
 [1]        [2]     [3]
Kadi tericɛ ya dereke
 [3] [2]        [1]

b. La nourriture du chat de la femme de Moussa
 [1]            [2]      [3]         [4]
domuni ya jakuma ya muso Musa <1>
 [1]             [2]     [3] [4]
Musa muso ya jakuma ya domuni
 [4] [3]       [2]           [1]

c. La voiture du père de l’ami du petit frère de Madou
 [1]        [2]      [3]      [4] [5]
mobili ya facɛ tericɛ dɔgɔcɛ Madu <1>
 [1]         [2] [3]      [4]       [5]
Madu dɔgɔcɛ tericɛ facɛ ya mobili
 [5] [4] [3] [2]            [1]
....

NOTE: les expressions précédées d’un astérisque (*) sont agrammaticales et donc fausses. <1>
<1> agrammaticales 

=== Exercice: Construire des groupes de mots

....
 [1]        [2]      [3]      [4] [5]
mobili ya facɛ tericɛ dɔgɔcɛ Madu <2>
  [1]         [2] [3]      [4]       [5]
Madu dɔgɔcɛ tericɛ facɛ ya mobili
 [5] [4] [3] [2]            [1]
....

NOTE: les expressions précédées d’un astérisque (*) sont agrammaticales et donc fausses.
<2> agrammaticales 

=== Exercice: Construire des groupes de mots

. La voiture de ma mère
. La main de mon amie
. les cheveux de Penda
. Le chien de Florance
. Le repas du chat
. La roue de la voiture
. L’école de ma fille
. Les oreilles du lapin
. La queue du mouton
. La maison du professeur
. Les amis de Coulibali
. Les amis de la sœur de Youssouf
. Les voitures du mari de ma soeur
. L’imam de la mosqué de Daresalam
. L’espace des vendeuses de Bouake
. Les bouchers du marché de Belleville

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
Voiture:: mobili
Mère:: bamuso
Main:: bolo
Amie:: terimuso
Cheveux:: kunsigi
Chien:: wulu
Repas:: domuni
Chat:: jakuma
Roue:: pine
a|
Ecole:: lakɔli
Fille:: denmuso
Oreille:: tolo
Lapin:: sozani
Queue:: kukala
Mouton:: saga
Maison:: bon
Professeur:: kalanfa, lakɔlifa
a|
Amis:: teriw
Soeur:: badenmuso
Mari:: furucɛ
Mosqué:: misiri
Imam:: limamu
Espace:: kɛnɛ
vendeuses:: feerekɛlaw
Bouchers:: sogofeerlaw
|====

