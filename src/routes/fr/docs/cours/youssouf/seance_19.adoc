= SÉANCE 19 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-08-04
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== LÉÇON 19 : KALANSEN TAN NI KƆNƆNTƆNAN

=== BARO SEEGINAN/THEME 8 :  à la gare d’Abidjan

.Contexte de communication
[abstract]
bɛ taga Abidjan bi.
A ka san tan ni looru ɲɔngɔn kɛ Bouaké nka bi, a b’a fɛ ka taga sigi Abidjan.
Coulibali ɲana, warisɔrɔ ka nɔgɔ Abidjan katimi Bouaké kan.
Abidjan ni Bouake cɛ ye kilo kɛmɛ saba ni dɔɔni.
A sori la ka bɔ jango a bɛ se à seyɔrɔ la jonan.
A ka à ya tike (mobilisarasɛbɛ) ta kabini kunu wulafɛ.

* I ni sɔgɔma
* N’ba YOUSSOUF, i ni sɔgɔma. Somɔgɔw do?
* O ka kɛnɛ kosɔbɛ. I bɛ taga mini ten ?
* N be taga Abidjan bi.
* O ka di wa?
* Ɔn-hɔn, o ka di kosɔbɛ. n’ba fɛ ka taga sigi Abidjan
* Ee n tericɛ, o tuman na Bouake ma di i ye tugun wa?
* O tɛ, n ya baara le bɛ n lase Abidjan. Yan, n tɛ nafa bɛrɛ bɛrɛ sɔrɔ la
* I tun ma kan ka kɔrɔtɔ. Ko bɛɛ n’a wagati.
* O ye can ye, nka n sanji tan ni looru le bɛ Bouake bi, kɔgɔ tɛ bɔ la nan na
* I bena mun baara (jumɛn) kɛ Abidjan, ɔridinatɛri baara wa?
* Ɔn-hɔn, n tɛ baara gbɛrɛ lɔn ni o tɛ ho

//-

* Allah ye yen diya i la katimi yan kan n tericɛ
* Amina
* I ka tike ta gari jumɛn na?
* "Abidjan express", o ya mobilisara sɔngɔ ma ca, o ya mobilikɔnɔyɔrɔw sanuyanin bɛ
* "Abidjan express" ya mobilisara ye joli ye ?
* Waga kelen ni kɛmɛ naani.
* N'i tun k’a fɔ n ɲana jonan n tun bena kow nɔgɔya i ye/bolo.
* Cogo di ?
* N kɔrɔcɛ taga la bi sɔgɔma jonan Abidjan a yɛrɛ ya mobili la. 
A tun bɛ se ka taga ɲɔgɔn fɛ
* Ee, o tun bena diya n ye kosɔbɛ nka basi tɛ. A bena bɛn
* Ka i ɲuman se/don
* Amina, n bena a wele ka Abidjan kibaroyaw di a ma.

Quelques minutes et c’est le depart (tuman dɔɔni, o ko tagawagati se la)

* A ye nan do jonan an bɛ taga
* N be se ka don ni n ya doniw bɛɛ ye kari/mobili kɔnɔ wa ?
* Ɔn-ɔn, i ya doniw ka ca kojugu. Paranticɛ bena o ladon mobilikɔfuru kɔnɔ
* A ye sabari ka n ya doniw bila yɔrɔ sanuman na dɛ !
* I kana jɔrɔ, n bena a fɔ à ɲana/ye.
* I n ice n tericɛ
* I ya tike bɛ mini?
* Tike filɛ. N ka kan ka sigi sofɛricɛ kɛrɛ fɛ
* O tɛ basi ye. I bɛ se ka sigi yɔrɔ min ka i diya. Yɛlɛ an bɛ taga.
