= Le singe et le lièvre
:author: unknown
:date: 2025-02-24
:keywords: dioula, jula, dyula, contes
:category: Julakan
:tags: Julakan, Language, Contes, Histoires
:description: Dioula conte à propos d'un singe et un lièvre
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

Il était une fois un singe et un lièvre. Ce singe et ce lièvre étaient
de bons amis au moment où l’éléphant donnait sa fille en mariage.
L’éléphant voulait donner sa fille à celui qui arriverait à lancer son
bâton dans l’arbre aux épines sans qu’il s’y accroche. Il appela tous
les animaux et leur annonça la nouvelle.
Le singe alla s’installer dans l’arbre aux épines. Personne ne le
voyait. Chaque animal passait à son tour et lançait son bâton dans
l’arbre. Le singe le retenait et cet animal était éliminé du jeu. Tous
lançèrent leurs bâtons… jusqu’au tour du lièvre. Le singe saisit le
bâton de son ami et le lui rejeta.
Tout le monde s’écria: «C’est Lièvre qui a gagné! C’est Lièvre qui a
gagné! C’est Lièvre qui a gagné!» Ainsi, l’éléphant donna cette énorme
femme au lièvre.
[arabic]
. Qu’est-ce que le singe et le lièvre ont fait?
. Est-ce gratuitement que le singe a aidé le lièvre?
. Accepteriez-vous d’aider gratuitement votre ami? Pourquoi?
