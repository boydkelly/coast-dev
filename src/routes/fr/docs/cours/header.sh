#!/usr/bin/bash
#Before runing this script ensure the doc has a valid asciidoctor title
#then set the type lang and author defaults

echo $0

[[ -z "$1" ]] && {
  echo "need a file"
  exit 1
}
# sed -n '/^=\s/q1' "$1" && {
#   echo "this looks like it doesn't have a title"
#   exit 1
# }
# export file=${1%%.adoc}.md
export file=$1
#delete blank lines
#sed -i '10,19{/^[[:space:]]*$/d}' "$1"

export title=$(sed -n 's/^= //p' "$1")
export author=$(awk /author:/{'first = $1; $1=""; print $0'} "$1" | sed 's/^ //g')
export category=$(awk /category:/{'first = $1; $1=""; print $0'} "$1" | sed 's/^ //g')
export description=$(awk /description:/{'first = $1; $1=""; print $0'} "$1" | sed 's/^ //g')
export tags=$(awk /tags:/{'first = $1; $1=""; print $0'} "$1" | sed 's/^ //g')
[[ -z $description ]] && description="$title"

[[ -z $author ]] && author="Soumahoro"
export date=$(awk '/date:/{print $2}' "$1" | uniq)
echo "$date"
[[ -z "$date" ]] && date=$(date -I)
export lang="fr"
export draft="true"
export pageNoindex="true"
[[ -z $category ]] && category="['Julakan']"
[[ -z $tags ]] && tags="['léçons']"
