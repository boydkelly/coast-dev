import { error } from '@sveltejs/kit';
import { processAsciiDoc } from '$lib/js/asciidoc.js';

export async function load() {
  try {
    const adocFile = await import(`./+page.adoc?raw`).catch(() => null);
    let attributes = {};

    if (adocFile?.default) {
      ({ metadata: attributes } = processAsciiDoc(adocFile.default)); // Corrected assignment
    }

    return {
      metadata: attributes
    };
  } catch {
    throw error(404, 'Not found');
  }
}
