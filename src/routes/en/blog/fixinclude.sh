#!/usr/bin/bash
#Before runing this script ensure the doc has a valid asciidoctor title
#then set the type lang and author defaults

includedir="{includedir}"

#sed -i 's/^:lang:/:page-lang:/g' $1
sed -n '/^:include::locale\s/q1' "$1" && echo ok found it
#sed -i "s/^:include::locale/:include::$includedir\/locale/g" "$1"
sed -i "s/include::locale/include::$includedir\/locale/g" $1
