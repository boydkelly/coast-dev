import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core';
const processor = Processor();

// this is needed to get the asciidoc attributes.
export const load = async ({ params }) => {
  const adocFile = await import(`../${params.slug}.adoc?raw`);
  if (adocFile.default) {
    const attributes = processor.load(adocFile.default).getAttributes();
    return {
      metadata: attributes
    };
  }
  error(404, 'Not found');
};
