import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core';
const processor = Processor();

export const load = async ({ params }) => {
  try {
    // Import the .adoc file (if it exists)
    const adocFile = await import(`../${params.slug}.adoc?raw`).catch(() => null);
    let attributes = {};

    if (adocFile?.default) {
      attributes = processor.load(adocFile.default).getAttributes();
    }
    // console.log(attributes)
    // Import the .svelte file (if it exists)
    const svelteFile = await import(`../${params.slug}.html?raw`).catch(() => null);

    if (!svelteFile?.default) {
      throw error(404, 'Not found');
    }
    // console.log(svelteFile.default)

    return {
      metadata: attributes,
      content: svelteFile.default
    };
  } catch {
    throw error(404, 'Not found');
  }
};
