import proverbs from '/src/assets/metadata/proverbs.json';
export async function load() {
  const randomProverb = proverbs[Math.floor(Math.random() * proverbs.length)];
  const dyu = randomProverb.dyu;
  const fr = randomProverb.fr;
  return {
    props: {
      dyu,
      fr,
    },
  };
}

