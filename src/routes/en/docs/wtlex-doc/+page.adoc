= Lexique WT mandenkan
:author: Boyd Kelly
:date: 2021-07-20
:category: Language
:sectnums:
:sectnumlevels: 1
:page-aliases:
:noindex:
:page-role: dico
:toc: macro
:toc-class: dico
:pdf-themesdir: ../_resources/themes
:pdf-theme: noto
:pdf-fontsdir: ../_resources/fonts
:lang: fr
include::{includedir}locale/attributes.adoc[]

[preface]
== Préface

Ce document est un outil d'apprentissage de Jula pour le champ de Côte d'Ivoire.
Il comprend un vocabulaire et expressions théocratiques en Jula de Burkina Faso.
Quelques expressions et mots équivalents sont inclus en Bambara, et parfois aussi des variantes 'ivoiriennes' aussi.
Ceci est un travail en cours.
Toutes les suggestions et ou corrections sont les bienvenus.

Cartes anki pour téléchargement
Index de recherche
hyperliens entres mots

.Abreviations
[width="50%",cols="^m,^m",frame="topbot",options="none",stripes="even"]
|====
2+a|
image::w100.svg[Jula wordcloud,width=100%]
|pd|parti du discours
|pr|pronom
|pr. réfl.| pronom réfléchi
|s.|singlulier
|pl.|pluriel
|adj.|adjectif
|n.|nom
|v.|verbe
|vt|verbe transitif
|vi|verbe intransitif
|conn. verb.|connectif verbal
|postp.|postposition
|mq. asp.|marque d'aspect
|mq. include.|marque de l'injonctif
|conn synt. compl.|connectif du systagme complétif
|mq. en. adj.|marque d'énoncé adjectival
|par. emph.|particule d'emphase
|mq. inact.|marque d'inactuel
|conn. distrib.|connectif du syntagme distributif
|part. phr.|particule phrastique
|pr. emph.|pronom emphatique
|cop.|copule
|fig.|figurée
|empl. idiom.|emploi idiomatque
|====

== Jula Français

include::{includedir}vocab-wtlex.adoc[leveloffset=+1]

