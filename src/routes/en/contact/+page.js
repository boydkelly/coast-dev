import { error } from '@sveltejs/kit';
import { processAsciiDoc } from '$lib/js/asciidoc.js';

export const load = async () => {
  const files = import.meta.glob('./*.adoc', { query: '?raw', import: 'default' });

  const entries = Object.values(files);
  if (entries.length !== 1) {
    error(404, 'Expected exactly one .adoc file in this directory');
  }

  const importer = entries[0];
  const rawContent = await importer();

  if (!rawContent) {
    error(404, 'Not found');
  }

  // **First pass: Extract metadata**
  const { metadata: attributes } = processAsciiDoc(rawContent);

  // Get the language code
  // const lang = attributes.lang || 'en'; // Default to 'en' if not set

  // **Second pass: Process with the correct language**
  const { content: html } = processAsciiDoc(rawContent);

  return {
    content: html,
    metadata: attributes // Keep as it was
  };
};
