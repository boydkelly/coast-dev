import fs from 'fs';
import path from 'path';

const BUILD_DIR = './build';
const OUTPUT_FILE = './src/lib/routes.json';

function getRoutes(dir, base = '') {
  let routes = [];
  const files = fs.readdirSync(dir);

  for (const file of files) {
    const fullPath = path.join(dir, file);
    const stat = fs.statSync(fullPath);

    if (stat.isDirectory()) {
      routes.push(...getRoutes(fullPath, `${base}/${file}`));
    } else if (file === 'index.html') {
      // Append trailing slash to all routes
      routes.push(`${base ? `${base}/` : '/'}`);
    }
  }

  return routes;
}

const routes = getRoutes(BUILD_DIR);
fs.writeFileSync(OUTPUT_FILE, JSON.stringify(routes, null, 2));

console.log(`Generated ${OUTPUT_FILE} with ${routes.length} routes.`);
