// sidenav.store.js
import { writable } from 'svelte/store';

export const isSideNavOpen = writable(false);
