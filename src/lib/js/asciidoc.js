import asciidoctor from '@asciidoctor/core';
import * as cheerio from 'cheerio';
const processor = asciidoctor();

// Load the document to extract attributes
export function processAsciiDoc(content, basedir = '.') {
  const doc = processor.load(content);
  const attributes = doc.getAttributes();
  const lang = attributes.lang || 'en';
  const includePath = `_include/${lang}/`;
  // console.log('includePath : ', includePath);
  // console.log('asciidoc.js: ', lang);
  const html = processor.convert(content, {
    basedir: basedir,
    standalone: false,
    safe: 'unsafe',
    attributes: {
      lang: lang,
      includedir: includePath,
      showtitle: true,
      imagesdir: '/images/',
      'allow-uri-read': '',
      'skip-front-matter': true,
      icons: 'font'
    }
  });
  const $ = cheerio.load(html, { decodeEntities: false }, false);

  // Fix tables
  $('table').each((_, table) => {
    const $table = $(table);

    if ($table.find('tbody').length === 0) {
      const tbody = $('<tbody></tbody>');
      $table.children('tr').each((_, tr) => tbody.append(tr));
      $table.append(tbody);
    }
  });

  // dont add carbon stying to asdiidoc horizontal list tables
  $('table')
    .not('.hdlist table')
    .each((_, table) => {
      const $table = $(table);
      $table.addClass('bx--data-table');
    });

  // Fix iframes
  $('iframe').each((_, iframe) => {
    const $iframe = $(iframe);
    if (!$iframe.attr('title')) {
      $iframe.attr('title', 'no_title');
    }
  });

  // Add Carbon Design classes to lists
  $('ul').addClass('bx--list--unordered bx--list--expressive');
  $('ol').addClass('bx--list--ordered bx--list--expressive');
  // $('ol').addClass('bx--list--ordered');
  $('ol').removeClass('arabic');
  $('ol').removeClass('alphanumeric');
  $('li').addClass('bx--list__item');
  // $('p').addClass('bx--type-body-02');

  // Replace href='#' with href='#top'
  $('a[href="#"]').each((_, element) => {
    $(element).attr('href', '#top');
  });

  return {
    // content: html,
    content: $.html(),
    metadata: attributes
  };
}
