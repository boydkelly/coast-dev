// baseurl.store.js
import { readable } from 'svelte/store';

export const baseUrl = readable('https://coastsystems.net');
