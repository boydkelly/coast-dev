// language.store.js
import { writable } from 'svelte/store';

const initialLang = (() => {
  if (typeof window !== 'undefined') {
    const userLanguage = navigator.language || navigator.userLanguage;
    return userLanguage.includes('fr') ? 'fr' : 'en';
  } else {
    // Handle the case when navigator is not defined (e.g., during server-side rendering)
    return 'en'; // Set a default language or choose an appropriate fallback
  }
})();

export var currentLang = writable(initialLang);

