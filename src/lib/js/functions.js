// In a separate file (lib/js/functions.js)
import lunr from 'lunr';

// functions.js (assuming ESM syntax)
export default function normaliseSpelling() {
  return function (builder) {
    const pipelineFunction = (token) => {
      // Your character mapping logic here
    if (token.toString() == "a") {
      return token.update(function () { return "à" })
    }
    if (token.toString() == "a") {
      return token.update(function () { return "á" })
    }
    if (token.toString() == "ny") {
      return token.update(function () { return "ɲ" })
    }
    if (token.toString() == "ò") {
      return token.update(function () { return "ɔ" })
    }
    if (token.toString() == "o") {
      return token.update(function () { return "ɔ" })
    }
    if (token.toString() == "o") {
      return token.update(function () { return "ɔ́" })
    }
    if (token.toString() == "o") {
      return token.update(function () { return "ɔ̀" })
    }
    if (token.toString() == "é") {
      return token.update(function () { return "e" })
    }
      // ...
      return token;
    };

    lunr.Pipeline.registerFunction(pipelineFunction, 'normaliseSpelling');

    builder.pipeline.before(lunr.stemmer, 'normaliseSpelling');
    builder.searchPipeline.before(lunr.stemmer, 'normaliseSpelling');
  };
}
