import routes from '$lib/routes.json';

export async function jsonRouteExists(route) {
  // console.log('route:', route, 'exists:', routes.includes(route));
  return routes.includes(route);
}

export async function routeExists(route) {
  try {
    const response = await fetch(route, { method: 'HEAD' });
    return response.ok;
  } catch (error) {
    return false;
  }
}

// Function to generate links for all valid routes in different languages
export async function getAllLanguageLinks(pathname) {
  const currentLang = pathname.split('/')[1];
  const currentPage = pathname.slice(pathname.indexOf(`/${currentLang}/`) + currentLang.length + 2);
  let languages = ['bm', 'dyu', 'en', 'fr', 'bci'];

  const validLanguageLinks = await Promise.all(
    languages.map(async (language, index) => {
      const route = `/${language}/${currentPage}`;
      const exists = await jsonRouteExists(route);
      if (exists) {
        return { link: route, language, index: index };
      }
      return null;
    })
  );

  return validLanguageLinks.filter((link) => link !== null);
}

// Function to get the index of a language in the languages array
export function getLanguageIndex(currentLang) {
  let languages = ['bm', 'dyu', 'en', 'fr', 'bci'];
  return languages.indexOf(currentLang); // Adding 1 to make it 1-based
}

// Function to generate links for valid routes in different languages
export async function getOtherLanguageLinks(pathname) {
  const currentLang = pathname.split('/')[1];
  const currentPage = pathname.slice(pathname.indexOf(`/${currentLang}/`) + currentLang.length + 2);
  let languages = ['bm', 'dyu', 'en', 'fr', 'bci'];

  const validLanguageLinks = await Promise.all(
    languages.map(async (language, index) => {
      if (language !== currentLang) {
        const route = `/${language}/${currentPage}`;
        const exists = await routeExists(route);
        if (exists) {
          return { link: route, language, index: index };
        }
      }
      return null;
    })
  );

  return validLanguageLinks.filter((link) => link !== null);
}
