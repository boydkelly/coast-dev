import { init, register } from 'svelte-i18n';
import { browser } from '$app/environment';

const defaultLocale = 'en';

const importLocale = async (locale) => {
  return import(`./locales/${locale}.json`);
};

register('en', () => importLocale('en'));
register('fr', () => importLocale('fr'));
register('bm', () => importLocale('bm'));
register('dyu', () => importLocale('dyu'));

// Initialize the i18n library
init({
  fallbackLocale: defaultLocale,
  //initialLocale: browser ? window.navigator.language : defaultLocale,
  initialLocale: browser
    ? window.navigator.language.includes('fr')
      ? 'fr'
      : defaultLocale
    : defaultLocale
});
