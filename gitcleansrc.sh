#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
git clean -fdx src/routes
