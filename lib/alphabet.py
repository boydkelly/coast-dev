#!/usr/bin/env python3
import sys
import os
import random

from os import path
from wordcloud import WordCloud

base16_colors = [
    "#08bdba",
    "#3ddbd9",
    "#78a9ff",
    "#ee5396",
    "#33b1ff",
    "#ff7eb6",
    "#42be65",
    "#be95ff",
    "#82cfff",
]

def base16_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    return random.choice(base16_colors)

# Determine the project root directory
project_root = path.abspath(path.join(path.dirname(__file__), '..'))

# Set the correct paths relative to the project root
output = path.join(project_root, "static/images/alphabet.svg")
stop = path.join(project_root, "src/stop.dyu.txt")
font_path = path.join(project_root, "_resources/fonts/notosans-regular.ttf")

# The text to generate the word cloud for
text = "a b c d e ɛ f g h i j k l m n ɲ ŋ o ɔ p r s t u v w y z"

# Load stopwords
stopwords = set()
with open(stop, "r") as file:
    for line in file:
        stripped_line = line.strip()
        stopwords.add(stripped_line)

# Create the word cloud
wc = WordCloud(
    background_color="black",
    stopwords=stopwords,
    collocations=False,
    min_word_length=1,
    repeat=False,
    relative_scaling=.2,
    width=1500,
    height=500,
    font_path=font_path,
    color_func=base16_color_func,  # Apply the custom color function
    max_words=50
)

wc.generate(text)

# Save the word cloud as an SVG file
wc_svg = wc.to_svg(embed_font=True)
with open(output, "w") as f:
    f.write(wc_svg)

