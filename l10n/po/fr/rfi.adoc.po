# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2025-03-06 13:09+0000\n"
"PO-Revision-Date: 2025-03-06 13:19+0000\n"
"Last-Translator: Boyd Kelly <bkelly@coastsystems.net>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Source-Language: en\n"
"X-Generator: Poedit 3.5\n"

#. type: Hash Value: locale
#: src/routes/en/blog/2020-04-23-clavier-android.adoc:3
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:6
#: src/routes/en/blog/2020-05-14-vimplug.adoc:8
#: src/routes/en/blog/2020-04-13-ponctuation.adoc:10
#: src/routes/en/blog/2020-10-21-fonts.adoc:10
#: src/routes/en/blog/2020-04-23-website.adoc:3
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:4
#: src/routes/en/blog/linkedin.adoc:4 src/routes/en/blog/font-test.adoc:4
#: src/routes/en/blog/asciidoc-quick-reference.adoc:38
#: src/routes/en/blog/2022-10-20-antora-po4a.adoc:12
#: src/routes/en/blog/2018-05-21-ysa.adoc:5
#: src/routes/en/blog/2018-05-22-datally.adoc:9
#: src/routes/en/blog/nvim-plugins.adoc:8 src/routes/en/blog/mogo.adoc:9
#: src/routes/en/blog/proverb.adoc:7 src/routes/en/blog/a-sen-b-a-la.adoc:5
#: src/routes/en/blog/2021-01-02-adoc-vim.adoc:9
#: src/routes/en/blog/2021-12-28-protein.adoc:12
#: src/routes/en/blog/2022-07-15-hunspell-fr.adoc:14
#: src/routes/en/blog/2022-07-18-hunspell-la.adoc:12
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:12
#: src/routes/en/blog/2022-01-04-antora.adoc:11
#: src/routes/en/blog/2016-04-21-justdoit.adoc:6
#: src/routes/en/blog/demo.adoc:10 _include/en/contact.adoc:4
#: src/routes/en/docs/w_jl/+page.adoc:10 _include/en/resources/mediaplus.adoc:7
#: _include/en/resources/ankataa.com.adoc:8
#: _include/en/resources/bebiphilip.adoc:9 _include/en/resources/bible.adoc:7
#: _include/en/resources/mandenkan.com.adoc:7 _include/en/resources/rfi.adoc:7
#: _include/en/resources/jw.org.adoc:8
#: _include/en/resources/fonts_google.adoc:7
#: _include/en/resources/fonts_ngalonci.adoc:6 src/routes/en/about/+page.adoc:5
#: src/routes/en/contact/contact.adoc:4
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "en"
msgstr "fr"

#. type: Attribute :description:
#: _include/en/resources/rfi.adoc:1 _include/en/resources/rfi.adoc:5
#, no-wrap
msgid "Radio France International Mandenkan"
msgstr "Radio France International Mandenkan"

#. type: Attribute :keywords:
#: _include/en/resources/rfi.adoc:6
msgid "Côte d'Ivoire, Ivory Coast, jula, julakan, dioula"
msgstr "Côte d'Ivoire, Ivory Coast, jula, julakan, dioula"

#. type: Positional ($1) AttributeList argument for macro 'image'
#: _include/en/resources/rfi.adoc:9
#, no-wrap
msgid "RFI"
msgstr "RFI"

#. type: Target for macro image
#: _include/en/resources/rfi.adoc:9
#, no-wrap
msgid "rfi_mandenkan.webp"
msgstr "rfi_mandenkan.webp"

#. type: Table
#: _include/en/resources/rfi.adoc:17
#, no-wrap
msgid ""
"2+|Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.\n"
"Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.\n"
"10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.\n"
"|Web site: link:https://rfi.fr/ma[rfi]|\n"
msgstr ""
"2+|Ecoutez l’actualité internationale, africaine, française et régionale (Afrique de l’Ouest) en mandenkan, ainsi que le sport, une revue de presse régionale. \n"
"Basé à Bambako, mais diffusé aux locuteurs mandenkan en Afrique de l'ouest. Mise-à-jour quotidiennement.  \n"
"Les infos en 10 minutes, suivi par une émission d'actualité de 20 minutes du lundi au vendredi à 8h00 et 12h00.\n"
"|Site web: link:http://rfi.fr/ma[rfi]\n"
