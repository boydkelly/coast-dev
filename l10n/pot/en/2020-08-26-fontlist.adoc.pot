# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2023-10-10 09:59+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/routes/en/blog/2020-08-26-fontlist.adoc:1
#, no-wrap
msgid "Web fonts for Mande languages"
msgstr ""

#. type: Table
#: src/routes/en/blog/2020-08-26-fontlist.adoc:14
#, no-wrap
msgid "|image:fontlist.webp['mandenkan font list',300,role='left']\n"
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2020-08-26-fontlist.adoc:16
#, no-wrap
msgid "List of all (Google) web fonts that are usable with Mande languages.footnote:[Other fonts are available for purchase; your application or web browser may also substitute fonts.]"
msgstr ""

#. type: Block title
#: src/routes/en/blog/2020-08-26-fontlist.adoc:18
#, no-wrap
msgid "Complete list of (Google) Web fonts for Mande languages."
msgstr ""

#. type: Table
#: src/routes/en/blog/2020-08-26-fontlist.adoc:22
#, no-wrap
msgid "include::{includedir}fontlist.tsv[]\n"
msgstr ""
