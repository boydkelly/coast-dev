# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2023-12-30 17:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/routes/en/blog/about.adoc:1 src/routes/en/docs/about.adoc:1
#: src/routes/en/about/+page.adoc:1
#, no-wrap
msgid "Who am I?"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/about.adoc:10 src/routes/en/docs/about.adoc:10
#: src/routes/en/about/+page.adoc:10
msgid ""
"In short, I have done extensive volunteer service including IT work, managed "
"the help desk for the Canadian Passport Office, and worked in a large "
"corporate IT department.  That’s experience in volunteer, public, and "
"private sectors."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/about.adoc:13 src/routes/en/docs/about.adoc:13
#: src/routes/en/about/+page.adoc:13
msgid ""
"I have hosted websites, managed email and dns servers, coded, and taught an "
"MS Exchange course at the University of British Columbia.  I have a lot of "
"experience in virtual technologies on Linux, Amazon and GCP."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/about.adoc:16 src/routes/en/docs/about.adoc:16
#: src/routes/en/about/+page.adoc:16
msgid ""
"This static website is hosted on Gitlab, and created by me using https://www."
"asciidoctor.org[Asciidoctor], https://antora.org[Antora], and lots of http://"
"neovim.io[neovim].  The DNS is hosted on GCP."
msgstr ""
