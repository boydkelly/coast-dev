# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2025-03-07 14:51+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Hash Value: About
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "About"
msgstr ""

#. type: Hash Value: Appearance
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Appearance"
msgstr ""

#. type: Hash Value: Back to top
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Back to Top"
msgstr ""

#. type: Hash Value: Bambara
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Bambara"
msgstr ""

#. type: Hash Value: Baoule
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Baoule"
msgstr ""

#. type: Hash Value: Blog
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Blog"
msgstr ""

#. type: Hash Value: Contents
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Contents..."
msgstr ""

#. type: Hash Value: Document language
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Document language"
msgstr ""

#. type: Hash Value: Dyula
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Dyula"
msgstr ""

#. type: Hash Value: Edit page
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Edit page..."
msgstr ""

#. type: Hash Value: Language
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "English"
msgstr ""

#. type: Hash Value: French
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "French"
msgstr ""

#. type: Hash Value: Next
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Next"
msgstr ""

#. type: Hash Value: Prev
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Prev"
msgstr ""

#. type: Hash Value: Search the docs
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Search the docs"
msgstr ""

#. type: Hash Value: Site language
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Site language"
msgstr ""

#. type: Hash Value: Study
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Study"
msgstr ""

#. type: Hash Value: usage
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Usage"
msgstr ""

#. type: Hash Value: wtlex author
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Boyd Kelly"
msgstr ""

#. type: Hash Value: wtlex category
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "julakan"
msgstr ""

#. type: Hash Value: alphabet description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Dyula alphabet with pronunciation examples for English speakers"
msgstr ""

#. type: Hash Value: alphabet example
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Jula example"
msgstr ""

#. type: Hash Value: alphabet ipa
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "IPA Symbol"
msgstr ""

#. type: Hash Value: alphabet keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "dioula, dyu, jula, alphabet"
msgstr ""

#. type: Hash Value: freq letter
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Letter"
msgstr ""

#. type: Hash Value: alphabet meaning
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "English meaning"
msgstr ""

#. type: Hash Value: alphabet page-image
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "cfa.webp"
msgstr ""

#. type: Hash Value: alphabet page-revdate
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "2024-01-13"
msgstr ""

#. type: Hash Value: alphabet pronounce
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "English pronunciation"
msgstr ""

#. type: Hash Value: alphabet title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Dyula alphabet and pronunciation guide for anglophones"
msgstr ""

#. type: Hash Value: currency
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Currency"
msgstr ""

#. type: Hash Value: fontlist description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "List of all Google fonts usable with Mande Lanuages including Bambara and Dyula (Dioula; Jula)"
msgstr ""

#. type: Hash Value: fontlist fontname
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Font Name"
msgstr ""

#. type: Hash Value: fontlist image
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "fontlist.svg"
msgstr ""

#. type: Hash Value: fontlist keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "fonts, webfonts, mande, mankenkan, dioula, dyula, jula, internet"
msgstr ""

#. type: Hash Value: fontlist sample
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Sample text"
msgstr ""

#. type: Hash Value: fontlist title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Web fonts usable with Mande Languages"
msgstr ""

#. type: Hash Value: freq description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "How many times does a Jula letter occur in the Watchtower since November 2020."
msgstr ""

#. type: Hash Value: freq keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "jula, watchtower, dioula, grammar, morphology"
msgstr ""

#. type: Hash Value: freq monthly
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Average Monthly Occurrances"
msgstr ""

#. type: Hash Value: wtlex page-image
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "w100.svg"
msgstr ""

#. type: Hash Value: freq tags
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "watchtower, jula"
msgstr ""

#. type: Hash Value: freq title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Frequency of Jula letters in the Watchtower"
msgstr ""

#. type: Hash Value: freq total
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Total Occurances"
msgstr ""

#. type: Hash Value: frequency
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Frequency"
msgstr ""

#. type: Hash Value: kodi_w100 description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "List of words most frequently used in Côte d'Ivoire Dyula"
msgstr ""

#. type: Hash Value: wjl_w100 keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "jula, dioula, dyula, ivory coast, language, lessons, grammar"
msgstr ""

#. type: Hash Value: wtlex tags
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "reference, julakan"
msgstr ""

#. type: Hash Value: kodi_w100 title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "100 Most Common Words in Côte d'Ivoire Dyula"
msgstr ""

#. type: Hash Value: locale
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "en"
msgstr ""

#. type: Hash Value: numeric
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Number"
msgstr ""

#. type: Hash Value: pdfdoc
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Document in pdf format"
msgstr ""

#. type: Hash Value: plugins category
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "technology"
msgstr ""

#. type: Hash Value: plugins description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "All installed (but necessarily enabled) Neovim plugins."
msgstr ""

#. type: Hash Value: plugins keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "tech, neovim, plugins"
msgstr ""

#. type: Hash Value: plugins page-image
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "neovim.svg"
msgstr ""

#. type: Hash Value: plugins plugin
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Plugin"
msgstr ""

#. type: Hash Value: plugins tags
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "tech"
msgstr ""

#. type: Hash Value: plugins title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "My current Neovim Plugins"
msgstr ""

#. type: Hash Value: pos
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Part of Speech"
msgstr ""

#. type: Hash Value: text_currency
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Written Currency"
msgstr ""

#. type: Hash Value: text_numeric
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Written Number"
msgstr ""

#. type: Hash Value: udhr-morph author
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "The Office of the High Commissioner for Human Rights"
msgstr ""

#. type: Hash Value: udhr-morph category
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Language"
msgstr ""

#. type: Hash Value: udhr-morph description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Morphology of the Universal Declaration of Human Rights in English"
msgstr ""

#. type: Hash Value: udhr-morph image
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "udhr-p.webp"
msgstr ""

#. type: Hash Value: udhr-morph keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Human rights, United Nations, udhr"
msgstr ""

#. type: Hash Value: udhr-morph title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Morphology of the Universal Declaration of Human Rights"
msgstr ""

#. type: Hash Value: wjl_w100 description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "List of words most frequently used in the Jula Watchtower"
msgstr ""

#. type: Hash Value: wjl_w100 title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "100 Most Common Jula Words in the Watchtower"
msgstr ""

#. type: Hash Value: word
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Word"
msgstr ""

#. type: Hash Value: wtlex description
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Compare theocratic terms in Dioula and Bambara"
msgstr ""

#. type: Hash Value: wtlex keywords
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "jula, dioula, dyula, ivory coast, language, lessons, grammar, bambara, mandenkan"
msgstr ""

#. type: Hash Value: wtlex title
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Bambara French Jula Theocratic dictionary"
msgstr ""
