# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2023-09-19 16:22+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: docs/en/modules/slides/pages/animal.adoc:6
#: docs/en/modules/slides/pages/money.adoc:6
#: docs/en/modules/slides/pages/colors.adoc:6
#: docs/en/modules/slides/pages/proverbs.adoc:6
#: content/en/modules/ROOT/pages/mogo.adoc:5
#: content/en/modules/ROOT/pages/a-sen-b-a-la.adoc:5
#: content/en/modules/ROOT/pages/2021-04-23-animal.adoc:9
#: content/en/modules/ROOT/pages/2020-08-26-fontlist.adoc:8
#, no-wrap
msgid "en "
msgstr ""

#. type: Attribute :category:
#: docs/en/modules/slides/pages/animal.adoc:10
#: docs/en/modules/slides/pages/money.adoc:10
#: docs/en/modules/slides/pages/5words.adoc:10
#: docs/en/modules/slides/pages/colors.adoc:10
#: docs/en/modules/slides/pages/proverbs.adoc:10
#, no-wrap
msgid "slides"
msgstr ""

#. type: Title =
#: docs/en/modules/slides/pages/proverbs.adoc:1
#, no-wrap
msgid "Proverb in Ivory Coast Jula"
msgstr ""

#. type: Attribute :description:
#: docs/en/modules/slides/pages/proverbs.adoc:2
#, no-wrap
msgid "Daily proverb in Ivory Coast Jula"
msgstr ""

#. type: Attribute :keywords:
#: docs/en/modules/slides/pages/proverbs.adoc:8
#: docs/en/modules/slides/pages/proverbs.adoc:9
#, no-wrap
msgid "\"jula\", \"africa\" ,\"dyu\", \"lessons\", \"revealjs\", \"proverb\""
msgstr ""

#. type: delimited block +
#: docs/en/modules/slides/pages/proverbs.adoc:14
#, no-wrap
msgid "<iframe src=\"https://slides-dyu.coastsystems.net/proverbs.html\" width=\"100%\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\" allow=\"geolocation *; microphone *; camera *; midi *; encrypted-media *\"></iframe>\n"
msgstr ""
