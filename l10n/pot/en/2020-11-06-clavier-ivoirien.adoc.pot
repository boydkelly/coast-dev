# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2025-03-06 13:09+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Hash Value: locale
#: src/routes/en/blog/2020-04-23-clavier-android.adoc:3
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:6
#: src/routes/en/blog/2020-05-14-vimplug.adoc:8
#: src/routes/en/blog/2020-04-13-ponctuation.adoc:10
#: src/routes/en/blog/2020-10-21-fonts.adoc:10
#: src/routes/en/blog/2020-04-23-website.adoc:3
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:4
#: src/routes/en/blog/linkedin.adoc:4 src/routes/en/blog/font-test.adoc:4
#: src/routes/en/blog/asciidoc-quick-reference.adoc:38
#: src/routes/en/blog/2022-10-20-antora-po4a.adoc:12
#: src/routes/en/blog/2018-05-21-ysa.adoc:5
#: src/routes/en/blog/2018-05-22-datally.adoc:9
#: src/routes/en/blog/nvim-plugins.adoc:8 src/routes/en/blog/mogo.adoc:9
#: src/routes/en/blog/proverb.adoc:7 src/routes/en/blog/a-sen-b-a-la.adoc:5
#: src/routes/en/blog/2021-01-02-adoc-vim.adoc:9
#: src/routes/en/blog/2021-12-28-protein.adoc:12
#: src/routes/en/blog/2022-07-15-hunspell-fr.adoc:14
#: src/routes/en/blog/2022-07-18-hunspell-la.adoc:12
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:12
#: src/routes/en/blog/2022-01-04-antora.adoc:11
#: src/routes/en/blog/2016-04-21-justdoit.adoc:6
#: src/routes/en/blog/demo.adoc:10 _include/en/contact.adoc:4
#: src/routes/en/docs/w_jl/+page.adoc:10 _include/en/resources/mediaplus.adoc:7
#: _include/en/resources/ankataa.com.adoc:8
#: _include/en/resources/bebiphilip.adoc:9 _include/en/resources/bible.adoc:7
#: _include/en/resources/mandenkan.com.adoc:7 _include/en/resources/rfi.adoc:7
#: _include/en/resources/jw.org.adoc:8
#: _include/en/resources/fonts_google.adoc:7
#: _include/en/resources/fonts_ngalonci.adoc:6 src/routes/en/about/+page.adoc:5
#: src/routes/en/contact/contact.adoc:4
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "en"
msgstr ""

#. type: Attribute :category:
#: src/routes/en/blog/2020-04-23-clavier-android.adoc:9
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:8
#: src/routes/en/blog/2020-05-14-vimplug.adoc:7
#: src/routes/en/blog/2020-04-13-ponctuation.adoc:9
#: src/routes/en/blog/2020-04-23-website.adoc:8
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:5
#: src/routes/en/blog/linkedin.adoc:3 src/routes/en/blog/font-test.adoc:5
#: src/routes/en/blog/asciidoc-quick-reference.adoc:6
#: src/routes/en/blog/2022-10-20-antora-po4a.adoc:9
#: src/routes/en/blog/2018-05-22-datally.adoc:8
#: src/routes/en/blog/nvim-plugins.adoc:7
#: src/routes/en/blog/2021-01-02-adoc-vim.adoc:8
#: src/routes/en/blog/2021-12-28-protein.adoc:10
#: src/routes/en/blog/2022-07-15-hunspell-fr.adoc:11
#: src/routes/en/blog/2022-07-18-hunspell-la.adoc:9
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:10
#: src/routes/en/blog/2022-01-04-antora.adoc:10 src/routes/en/blog/demo.adoc:8
msgid "Technology"
msgstr ""

#. type: Title =
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:1
#, no-wrap
msgid "Ivory Coast Keyboard  no gymnastics"
msgstr ""

#. type: Attribute :description:
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:5
msgid "Type like a boss in Linux with Ivorian Languages!"
msgstr ""

#. type: Attribute :tags:
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:7
msgid "language, tech, africa"
msgstr ""

#. type: Attribute :keywords:
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:9
msgid ""
"jula, julakan, dioula, dyula, dyu, international, keyboard, android, baoule, "
"\"ivory coast\", \"côte d'Ivoire\", bete, senoufo, language, \"afriacn "
"languages\", africa, technology, multilingual, french, english, linux, "
"fedora, ubuntu, alpine, suse, mint, keyboard-config, xkeyboard"
msgstr ""

#. type: Target for macro image
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:13
#, no-wrap
msgid "clavier_ivoirien.webp"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:16
msgid ""
"I am very pleased to present the 'Ivorian Keyboard' on public repos pour rpm "
"and deb based linux distributions. (Others like Arch, Alpine, Gentoo may "
"follow). This keyboard allows '_easy_' typing (*without doing finger "
"gymnastics*) of Ivorian languages."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:18
msgid ""
"Many multilingual keyboards require key combinations that interrupt the "
"'flow' of typing. This keyboard for the most common characters allows the "
"hands to remain positioned normally on the keyboard. In addition capital and "
"small case have the same 'movements' facilitating muscle memory. This is "
"very helpful for translators and writers. The package also includes a "
"standard french azerty keyboard \"Français (Côte d'Ivoire)\", a multilingual "
"layout \"Côte d'Ivoire Multilingue\", and a Qwerty layout, based on the "
"(excellent) French Canadian keyboard \"Côte d'Ivoire Querty\"."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:20
msgid ""
"The keyboard can be installed on rpm systems from Fedora, Suse, Mageia etc, "
"and deb based Ubuntu varieties. Just add the appropriate copr or ppa repo "
"and update either the keyboard package or your system."
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:21
#: src/routes/en/blog/2022-07-15-hunspell-fr.adoc:33
#: src/routes/en/blog/2022-07-18-hunspell-la.adoc:28
#, no-wrap
msgid "Installation"
msgstr ""

#. type: Title ===
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:23
#, no-wrap
msgid "Fedora (and Mageia):"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:26
msgid ""
"Repo: link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-"
"config-ci/[Fedora]"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:31
#, no-wrap
msgid ""
"sudo dnf copr enable boydkelly/xkeyboard-config-ci\n"
"sudo dnf update xkeyboard-config\n"
msgstr ""

#. type: Title ===
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:33
#, no-wrap
msgid "Suse:"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:36
msgid ""
"Repo: link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-"
"config-ci/[Suse]"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:38
msgid ""
"Use Yast and add link:https://copr.fedorainfracloud.org/coprs/boydkelly/"
"xkeyboard-config-ci/"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:42
#, no-wrap
msgid "zypper add https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/\n"
msgstr ""

#. type: Title ===
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:44
#, no-wrap
msgid "Debian/Ubuntu distributions:"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:47
msgid ""
"Repo: link:https://launchpad.net/~bkelly/+archive/ubuntu/xkb-data-ci[Ubuntu]"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:52
#, no-wrap
msgid ""
"sudo add-apt-repository ppa:bkelly/xkb-data-ci\n"
"sudo  upgrade xdb-data\n"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:55
msgid ""
"After the repository is added and the package updated, you will find the "
"keyboards available under the language input settings of your system."
msgstr ""

#. type: Table
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:61
#, no-wrap
msgid ""
"|Search for 'french' or 'Côte d'Ivoire' and you will find them.|Switch between available keyboards.\n"
"a|image:input_lang.webp[]\n"
"a|image:switch_lang.webp[]\n"
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:63
#, no-wrap
msgid "Notes:"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:66
msgid ""
"If you have patches or contributions, see the code on link:https://gitlab."
"com/boydkelly/xkeyboard-config-ci"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:68
msgid ""
"The Ivory Coast French layout is identical to the French national azerty "
"keyboard."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:70
msgid ""
"The Ivory Coast multilingual (civ) keyboard est based on the French azerty "
"keyboard, but includes supplemental Unicode characters for the languages of "
"Ivory Coast. This keyboard has been based on those from Togo, Nigeria, Mali "
"and in particular Cameroon. It can be used to type Attié, Abé, Bambara, "
"Baoulé, French, Gueré, Jula, Senoufo, Yacouba, and other languages."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:72
msgid "The Unicode characters are accessible in two ways:"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:74
msgid ""
"Press and hold the kbd:[AltGr] followed by the appropriate key in the "
"following table."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:75
msgid ""
"Press the kbd:[!] (on the azerty keyboard), release and then press the "
"appropriate key from the table below."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:77
msgid ""
"The first method (found on many other keyboards) may be difficult, "
"especially when the key in question is a capital letter on the right of the "
"keyboard."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:79
msgid "The second method (preferable) allows more fluid typing."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:81
msgid "Press the kbd:[!] twice to actually insert the exclamation mark."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:83
msgid ""
"For some more rare cases for example a grave or aigu accent on ɛ́ ɛ̀ or any "
"other letter, type the ɛ with the kbd:[!] (azerty) or ; (querty) followed by "
"kbd:[AltGr] + kbd:[,] or kbd:[.] or kbd:[/]."
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:105
#, no-wrap
msgid ""
"   ____\n"
"  | 1 3| 1 = Shift,  3 = AltGr + Maj    (AltGr is on the right)\n"
"  | 2 4| 2 = normal, 4 = AltGr\n"
"   ¯¯¯¯\n"
"   ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ _______\n"
"  |    | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 0  | °  | +  | <--   |\n"
"  | ²  | &  | é ~| \" #| ' {| ( [| - || è `| _ \\| ç ^| à @| ) ]| = }|       |\n"
"   ========================================================================\n"
"  | |<-  | A  | Z Ʒ| E Ɛ| R  | T  | Y Ƴ| U Ʋ| I Ɩ| O Ɔ| P  | ¨  | $  |   , |\n"
"  |  ->| | a  | z ʒ| e ɛ| r  | t  | y ƴ| u ʋ| i ɩ| o ɔ| p  | ^  ̌| £ ¤| <-' |\n"
"   ===================================================================¬    |\n"
"  |       | Q  | S  | D Ɗ | F  | G Ŋ| H  | J  | K  | L  | M  | %  | µ  |   |\n"
"  | SHIFT   | q  | s  | d ɗ | f  | g ŋ| h  | j  | k  | l  | m  | ù `| *  ́|   |\n"
"   ========================================================================\n"
"  | ^   | >  | W  | X  | C  | V Ʋ| B Ɓ| N Ŋ| ?  | .  | /  | §  |     |     |\n"
"  | |   | <  | w  | x  | c  | v ʋ| b ɓ| n ŋ| ,  | ;  | : ¯| ! ~|     |     |\n"
"   ========================================================================\n"
"  |      |      |      |                       |       |      |     |      |\n"
"  | Ctrl | Super| Alt  | Space   Nobreakspace | AltGr | Super|Menu | Ctrl |\n"
"   ¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯ ¯¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯ ¯¯¯¯¯\n"
"----  \n"
msgstr ""
