# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2025-03-06 13:09+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Hash Value: locale
#: src/routes/en/blog/2020-04-23-clavier-android.adoc:3
#: src/routes/en/blog/2020-11-06-clavier-ivoirien.adoc:6
#: src/routes/en/blog/2020-05-14-vimplug.adoc:8
#: src/routes/en/blog/2020-04-13-ponctuation.adoc:10
#: src/routes/en/blog/2020-10-21-fonts.adoc:10
#: src/routes/en/blog/2020-04-23-website.adoc:3
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:4
#: src/routes/en/blog/linkedin.adoc:4 src/routes/en/blog/font-test.adoc:4
#: src/routes/en/blog/asciidoc-quick-reference.adoc:38
#: src/routes/en/blog/2022-10-20-antora-po4a.adoc:12
#: src/routes/en/blog/2018-05-21-ysa.adoc:5
#: src/routes/en/blog/2018-05-22-datally.adoc:9
#: src/routes/en/blog/nvim-plugins.adoc:8 src/routes/en/blog/mogo.adoc:9
#: src/routes/en/blog/proverb.adoc:7 src/routes/en/blog/a-sen-b-a-la.adoc:5
#: src/routes/en/blog/2021-01-02-adoc-vim.adoc:9
#: src/routes/en/blog/2021-12-28-protein.adoc:12
#: src/routes/en/blog/2022-07-15-hunspell-fr.adoc:14
#: src/routes/en/blog/2022-07-18-hunspell-la.adoc:12
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:12
#: src/routes/en/blog/2022-01-04-antora.adoc:11
#: src/routes/en/blog/2016-04-21-justdoit.adoc:6
#: src/routes/en/blog/demo.adoc:10 _include/en/contact.adoc:4
#: src/routes/en/docs/w_jl/+page.adoc:10 _include/en/resources/mediaplus.adoc:7
#: _include/en/resources/ankataa.com.adoc:8
#: _include/en/resources/bebiphilip.adoc:9 _include/en/resources/bible.adoc:7
#: _include/en/resources/mandenkan.com.adoc:7 _include/en/resources/rfi.adoc:7
#: _include/en/resources/jw.org.adoc:8
#: _include/en/resources/fonts_google.adoc:7
#: _include/en/resources/fonts_ngalonci.adoc:6 src/routes/en/about/+page.adoc:5
#: src/routes/en/contact/contact.adoc:4
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "en"
msgstr ""

#. type: Title =
#: _include/en/contact.adoc:1 src/routes/en/contact/contact.adoc:1
#, no-wrap
msgid "Contact"
msgstr ""

#. type: delimited block +
#: _include/en/contact.adoc:9
#, no-wrap
msgid "<iframe title=\"contact\" src=\"https://docs.google.com/forms/d/e/1FAIpQLSdw6yhla0-mmVrAWeLcHM2lBKHvKZre4uiiiGCjvaG30x22Qg/viewform?embedded=true\" width=\"auto\" height=\"1000\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\"></iframe>\n"
msgstr ""
