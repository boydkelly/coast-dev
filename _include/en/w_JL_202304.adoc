.Kɔrɔsili Sangaso, saan 2023, awirilikalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202304.svg[w_JL_202304,300]

* Total words 18248



* Unique words 1029



* Plural words 892



* Words to be confirmed 0
a|

. t’a
. nan
. kafo
. n’i
. kama
. hakili
. tɛna
. ɲi
. kabako
. wa
a|
* sebagayabɛɛtigi
* dusukasibagatɔw
* tilenbaliyakow
* sɛgɛsɛgɛrikɛla
* limaniyabaliya
* denmusofitinin
* weleweledalaw
* sukasibagatɔw
* sɛtanburukalo
* novanburukalo
|====
