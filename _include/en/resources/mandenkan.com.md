# https://mandenkan.com [Mandenkan]

![mandenkan.com](logo-mandenka.webp)

This web site is produced by a true linuguistic expert, and native speaker, who also offers personal tutoring in Abidjan and distance learning.
He has an excellent command of grammar, and knows the challenges of a learner faced with the Mandenkan thought schema.

|Web site: [Mandenkan](https://mandenkan.com)|Contact: contact@mankenkan.com
