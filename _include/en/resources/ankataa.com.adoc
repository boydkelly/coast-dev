= Le site web https://www.ankataa.com[Ankataa]
:author: Boyd Kelly
:email:
:date: 2020-05-08
:description: Dyula/Dioula study resources
:tags: "jula", "language"
:keywords: "Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang: en

image::ankataa.webp[ankataa,300,role=left]

[role="grid", width="100%",cols="3,4,3",frame="topbot",options="none",stripes="none"]
|====
3+|An expert linguist providing online courses, in depth research as well an awesome Youtube channel for learning basic Jula and Bambara.
Great for Jula learniers in Ivory Coast. (Just substitute 'ka' for ye, and 'le' or 'lo' for 'don'...  and a few others but after all this *is* mandenkan!)
|Web site:link:https://www.ankataa.com[Ankataa]||Youtube channel:link:https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw[Na baro kɛ]
|====

