= Radio Media+ in the heart of Bouaké (FM 103)
:author: Boyd Kelly
:email:
:date: 2020-05-08
:description: Jula learners resources 
:keywords: "Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula 
:lang: en

image::radiomediaplus.webp[radiomedia+,300,role=left]

[role="grid" width="100%",cols="4",frame="topbot",options="header",stripes="none"]
|====
4+|Great resource for picking up the local accent, improving oral comprehension, and augmenting vocabulary. +
FM 103 broadcasting in the Jula language at the following times:
|Day|Time|Host|Program
|Tuesday|9h-10h30||Social issues
|Wednesday|9h-10h30||Social issues
|Sunday|9h-10h30||Social issues
|https://www.facebook.com/RADIOMEDIAPLUSCIBOUAKE[Facebook]
|Phone: +225 31 63 12 69
2+|Email: mediaplusci@yahoo.fr
|====
