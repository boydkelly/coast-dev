# JW.org in Bambara, French and Jula 

|     |
| --- 3+ |
| These sites contain articles, videos and audio tracks in Burkina Faso Jula, that you can compare to the same in Bambara or English. |
| [jw.org in Bambara](https://www.jw.org/bm)  |
| [jw.org in French](https://www.jw.org/fr) |
| [jw.org in jula](https://www.jw.org/dyu)  |
