# Font sanity check for Bambara, Jula (and other manding languages)

The internet is international, but not all fonts can display characters from Afrian languages.
[Here is a page](https://www.coastsystems.net/dyu/ngalonci) where you can check out how a font looks and if it will display the necessary characters used in Manding languages.
