# Google web fonts for manding languages

![polices web](polices.webp)

If your are an African web developer go to [Google Fonts](https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom) `
to check if your favorite web font contains the appropriate unicode characters to display your language.
