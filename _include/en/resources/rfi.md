# Radio France International Mandenkan

![RFI](rfi_mandenkan.webp)

Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.

Based in Bambako, but broadcast to Mandenkan listeners across West Africa. Updated daily.
10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.

Web site: [rfi](https://rfi.fr/ma)|
