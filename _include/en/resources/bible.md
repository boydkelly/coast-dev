# The Bible

![Bible CI dioula](bibleci.webp)

Compare the text of the Bible in Ivory Coast and Burkina Faso Jula with the French Louis Ségon version.

[Bible en Dioula de Côte d’Ivoire](https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible)
[Bible en Dioula de Burkina Faso](https://play.google.com/store/apps/details?id=com.dioula.parolededieu.burkinafaso)
