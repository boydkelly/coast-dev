.Kɔrɔsili Sangaso, saan 2022, novanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202211.svg[w_JL_202211,300]

* Total words 16847



* Unique words 1055



* Plural words 892



* Words to be confirmed 0
a|

. daminɛ
. ale
. wala
. tora
. kɔ
. senu
. ninsɔndiya
. nan
. don
. ɲɔgɔn
a|
* sɛgɛsɛgɛrikɛlaw
* alaɲasiranbagaw
* tilenbaliyakow
* sɛgɛsɛgɛrikɛla
* sarakalasebaga
* ɔriganisasiyɔn
* mɔgɔmurutininw
* kunkanbaarabaw
* kabakurugwɛlɛn
* dɔnkililabagaw
|====
