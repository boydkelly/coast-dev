.Kɔrɔsili Sangaso, saan 2023, zuwɛnkalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202306.svg[w_JL_202306,300]

* Total words 16564



* Unique words 1140



* Plural words 961



* Words to be confirmed 0
a|

. mɔgɔw
. hakilitigiya
. barokun
. jaa
. sera
. ɲɛsiran
. t’a
. sɔn
. sabu
. juman
a|
* hakilitigiyasira
* sɛgɛsɛgɛrikɛla
* sarakalasebaga
* ɔriganisasiyɔn
* hakilitigiyako
* sɔrɔdasiɲɔgɔn
* sinɔgɔbagatɔw
* sɛtanburukalo
* ninsɔndiyanin
* ninsɔndiyakow
|====
