.Sangaso, saan 2022, sɛtanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202209.svg[w_JL_202209,300]

* Total words 17740



* Unique words 1034



* Plural words 887



* Words to be confirmed 0
a|

. n’an
. kuma
. bɔ
. banbali
. diinan
. wɛrɛw
. nin
. lannamɔgɔ
. ɲɔgɔn
. b’an
a|
* tilenninyatigi
* tilenbaliyakow
* ɔriganisasiyɔn
* ninsɔndiyakoba
* kunkanbaarabaw
* weleweledalaw
* sɛtanburukalo
* politikimɔgɔw
* ladiligwɛlɛnw
* kunkanbaaraba
|====
