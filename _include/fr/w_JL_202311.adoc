.Kɔrɔsili Sangaso, saan 2023, novanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202311.svg[w_JL_202311,300]

* Nombre total de mots 16621



* Mots uniques 1093



* Mots pluriels 941



* Mots à confirmer 13
a|

. aw
. m’a
. jira
. sa
. kelen
. deliliw
. wala
. waajuli
. mɔgɔw
. daminɛ
a|
* waajulikɛɲɔgɔn
* sɛgɛsɛgɛrikɛla
* ɔriganisasiyɔn
* weleweledalaw
* tilenninyakow
* novanburukalo
* kɛrɛnkɛrɛnnin
* kerecɛnɲɔgɔnw
* desanburukalo
* dafabaliyakow
|====
