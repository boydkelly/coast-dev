.Kɔrɔsili Sangaso, saan 2024, marisikalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202403.svg[w_JL_202403,300]

* Nombre total de mots 36303



* Mots uniques 1448



* Mots pluriels 1221



* Mots à confirmer 60
a|

. tiɲɛn
. n’o
. jogo
. mɔgɔw
. hali
. fɛɛn
. b’o
. barika
. loon
. kosɔn
a|
* tilenbaliyakow
* ɔriganisasiyɔn
* mɔgɔsabalininw
* kunkanbaarabaw
* hakilitigiyako
* hakilintanyako
* weleweledalaw
* torofuraburuw
* tilenbaliyako
* ɲumanlɔnbaliw
|====
