image::udhr-morph.bam.svg[UDHR,310]
[horizontal]
Nombre total de mots:: 1659



[horizontal]
Mots uniques:: 464




//- 

[]
*  Échantillon de vocabulaire



. hɔrɔnɲasira
. sariyabolo
. sintin
. kabila
. lafiɲɛ
. lakanasira
. maralen
. jateli
. kabilaya
. jate


//- 

[]
*  Mots les plus longs



** fisamanciyawalew
** tɔgɔlafɛntigiya
** sariyabatufanga
** sagolasugandili
** mɔgɔlaɲiniwale
** tɔndenjamanaw
** kɛrɛnkɛrɛnnen
** jɛnsɛnnifɛɛrɛ
** tɔndenjamana
** sɛmɛntiyalen
