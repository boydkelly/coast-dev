.Kɔrɔsili Sangaso, saan 2023, zuwekalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202307.svg[w_JL_202307,300]

* Nombre total de mots 18497



* Mots uniques 1180



* Mots pluriels 979



* Mots à confirmer 0
a|

. sɔn
. n’u
. kerecɛn
. aw
. yɔrɔ
. bɔ
. hakili
. fɔɔ
. nin
. misali
a|
* farikoloɲanagwɛla
* dugukoloyɛrɛyɛrɛ
* sɛgɛsɛgɛrikɛlaw
* farikoloɲanagwɛ
* tobilikɛminanw
* sɛgɛsɛgɛrikɛla
* ɔriganisasiyɔn
* mɔgɔsabalininw
* hakilitigiyako
* antereperenɛri
|====
