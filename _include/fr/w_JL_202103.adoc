.Kɔrɔsili Sangaso, saan 2021, marisikalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202103.svg[w_JL_202103,300]

* Nombre total de mots 19102



* Mots uniques 1153



* Mots pluriels 957



* Mots à confirmer 0
a|

. b’o
. nan
. gwɛlɛya
. bibulukalan
. saan
. n’u
. latanga
. kanuya
. kama
. hali
a|
* sɔrɔdasiɲɔgɔnw
* sarakalasebaga
* ɔriganisasiyɔn
* kunkanbaarabaw
* weleweledalaw
* sɔlidasiɲuman
* ngalontigɛlaw
* nɛgɛkunbɛnnan
* kunkanbaaraba
* kɛrɛnkɛrɛnnin
|====
