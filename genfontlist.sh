#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
export fontlist_file=/home/bkelly/dev/fontlist/fontlist.txt
export base="https://fonts.google.com/specimen/"
export text="M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye"
export project=fontlist
export image_type=webp
for x in json adoc tsv svg webp; do export "${x}=${project}.$x"; done
#output_tsv="/home/bkelly/dev/svelte/coastsystems.net/src/_include/fontlist.tsv"
#sample link
#link:https://fonts.google.com/specimen/Judson?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom#standard-styles[Judson]
#linktest=linktest.tsv
mk_json() {
  # Initialize an array for JSON objects
  font_array=()

  while read -r fontname; do
    pretty=$(echo "$fontname" | tr '_' ' ') && pretty=${pretty%-*}
    urlfont=$(echo "$fontname" | tr '_' '+') && urlfont=${urlfont%-*}
    image=${fontname%.ttf}.${image_type}
    src="<img src='/images/$image' alt='$pretty' />"

    #echo $urlfont
    # create link test and/or tsv
    printf '%s%s?preview.text=%s&preview.text_type=custom#standard-styles[%s,window=_blank]\n' $base $urlfont $text "$pretty" >>"$tsv"
    rm $tsv #no need for now?
    # 	#printf "%s%s?preview.text=%s&preview.text_type=custom#standard-styles,\n" "$base" "$urlfont" "$text" >> $linktest
    # Create a JSON object and append it to the array
    object=$(jq -n --arg base "$base" --arg urlfont "$urlfont" --arg pretty "$pretty" --arg text "$text" --arg src "$src" --arg image "$image" \
      '{ "url": "\($base)\($urlfont)?preview.text=\($text)&preview.text_type=custom#standard-styles[\($pretty),window=_blank]",
      "pretty": "\($pretty)",
      "src": "\($src)",
      "image": "\($image)"
    }')

    font_array+=("$object")
  done < <(sort "$fontlist_file")

  # Print the array with commas between elements and enclose it in []
  #printf "[%s]" "${json_array[*]/%/,}" >$output_json
  target=$HOME/dev/svelte/coastsystems.net/src/assets/metadata/$json
  #printf "[%s]" "${json_array[*]/%/,}" | sed 's/\},\]/\}\]/' | jq | yq -pj -oj -I0 ".[] |= (.id = .pretty)" >$target
  printf "[%s]" "${font_array[*]/%/,}" | sed 's/\},\]/\}\]/' | jq | yq -pj -oj ".[] |= (.id = .pretty)" >$target
  #printf "[%s]" "${json_array[*],}"
  # Check if there are changes after processing
  true
}

mk_images() {
  # lets make the images now
  image_text="Mɔgɔkɔrɔbaw kaɲi ka kɛ ni ŋania ɲuman ye"

  font_location=./_resources/fonts
  # Directory to save the output images
  output_dir="./static/images"

  width=600
  height=50
  font_size=22

  # Create the output directory if it doesn't exist
  mkdir -p "$output_dir"

  # Read and sort the list of fonts from fontlist.txt
  while IFS= read -r font_name; do
    if [ -z "$font_name" ]; then
      continue
    fi

    # Generate output file name
    output_file="$output_dir/$(basename "$font_name" .ttf).$image_type"
    # svg_file="$output_dir/$(basename "$font_name" .ttf).svg"

    magick -size "${width}x${height}" xc:white \
      -font "$font_location/$font_name" \
      -pointsize $font_size \
      -gravity center \
      -annotate +0+0 "$image_text" \
      "$output_file"

    # magick -size "${width}x${height}" \
    #   -background white \
    #   -fill black \
    #   -font "$font_location/$font_name" \
    #   -pointsize $font_size \
    #   -gravity center \
    #   "label: $image_text" \
    #   "$svg_file"

    echo "Saved image for $font_name as $output_file"
  done < <(sort "$fontlist_file")
}

mk_json
mk_images
